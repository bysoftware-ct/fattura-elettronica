package it.bysoftware.ct.electronicinvoice;

import it.bysoftware.ct.electronicinvoice.body.ElectronicInvoiceBody;
import it.bysoftware.ct.electronicinvoice.header.ElectronicInvoiceHeader;
import it.bysoftware.ct.electronicinvoice.utils.ElectronicInvoiceVersion;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * @author Aliseo-G
 *
 */
@XmlRootElement(name = "FatturaElettronica",
    namespace = "http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2"
)
@XmlType(propOrder = { "header", "body" })
public class ElectronicInvoice {
    
    /**
     * Electronic invoice version.
     */
    private ElectronicInvoiceVersion version;

    /**
     * 
     */
    private ElectronicInvoiceHeader header;

    /**
     * 
     */
    private List<ElectronicInvoiceBody> body;

    /**
     * Returns version type.
     * 
     * @return the version
     */
    @XmlAttribute(name = "versione")
    public final String getVersion() {
        return this.version.getVal();
    }

    /**
     * Sets the version type, {@link ElectronicInvoiceVersion}.
     * 
     * @param ver
     *            the version
     */
    public final void setVersion(final ElectronicInvoiceVersion ver) {
        this.version = ver;
    }

    /**
     * @return the header
     */
    @XmlElement(
            name = "FatturaElettronicaHeader"
    )
    public final ElectronicInvoiceHeader getHeader() {
        return this.header;
    }

    /**
     * Sets the invoice header.
     * 
     * @param h
     *            the invoice header to set
     */
    public final void setHeader(final ElectronicInvoiceHeader h) {
        this.header = h;
    }

    /**
     * Returns the list of invoices bodies.
     * 
     * @return the list list of invoices bodies
     */
    @XmlElement(name = "FatturaElettronicaBody")
    public final List<ElectronicInvoiceBody> getBody() {
        return body;
    }

    /**
     * @param list
     *            the list to set
     */
    public final void setBody(final List<ElectronicInvoiceBody> list) {
        this.body = list;
    }

}
