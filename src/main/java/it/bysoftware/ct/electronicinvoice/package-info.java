/**
 * 
 */
/**
 * @author Aliseo-G
 *
 */
@XmlSchema(xmlns = {
        @XmlNs(namespaceURI = "http://ivaservizi.agenziaentrate.gov.it/docs/xsd"
                + "/fatture/v1.2",
            prefix = "p"
        ),
        @XmlNs(namespaceURI = "http://www.w3.org/2000/09/xmldsig#",
            prefix = "ds"
        )
})
package it.bysoftware.ct.electronicinvoice;

import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.XmlNs;

