/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.header;

/**
 * @author Aliseo-G
 *
 */
public final class IssuerData extends SubjectData {

    /**
     * 
     */
    private IssuerData() {
    }

    /**
     * @param builder  the builder
     */
    private IssuerData(final IssuerDataBuilder builder) {
        super(builder);
    }

    /**
     * @author Aliseo-G
     *
     */
    public static final class IssuerDataBuilder extends
        SubjectData.SubjectDataBuilder<IssuerDataBuilder> {

        /**
         * 
         * @param data
         *            the {@link PersonalData}
         */
        public IssuerDataBuilder(final PersonalData data) {
            super(data);
        }
        
        /**
         * Builds the {@link IssuerData} object.
         * 
         * @return the new {@link IssuerData} object
         */
        public IssuerData build() {
            return new IssuerData(this);
        }

    }
}
