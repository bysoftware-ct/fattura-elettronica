/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.header;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = { "fiscalId", "fiscalCode", "company",
        "professionalRegister", "registerProvince", "registerRegNumber",
        "registerRegDate", "taxRegime" })
public final class PersonalData {

    /**
     * 
     */
    private transient FiscalId fiscalId;

    /**
     * 
     */
    private transient String fiscalCode;

    /**
     * 
     */
    private transient CompanyData company;

    /**
     * 
     */
    private transient String professionalRegister;

    /**
     * 
     */
    private transient String registerProvince;

    /**
     * 
     */
    private transient String registerRegNumber;

    /**
     * 
     */
    private transient String registerRegDate;

    /**
     * 
     */
    private transient String taxRegime;

    /**
     * 
     */
    private PersonalData() {
        super();
    }

    /**
     * 
     * @param builder
     *            builder
     */
    protected PersonalData(final PersonalDataBuilder builder) {
        this();
        this.fiscalId = builder.getFiscalId();
        this.fiscalCode = builder.getFiscalCode();
        this.company = builder.getCompany();
        this.professionalRegister = builder.getProfessionalRegister();
        this.registerProvince = builder.getRegisterProvince();
        this.registerRegNumber = builder.getRegisterRegNumber();
        this.registerRegDate = builder.getRegisterRegDate();
        this.taxRegime = builder.getTaxRegime();
    }

    /**
     * @return the fiscalId
     */
    @XmlElement(name = "IdFiscaleIVA")
    public FiscalId getFiscalId() {
        return this.fiscalId;
    }

    /**
     * @return the fiscalCode
     */
    @XmlElement(name = "CodiceFiscale")
    public String getFiscalCode() {
        return this.fiscalCode;
    }

    /**
     * @return the company
     */
    @XmlElement(name = "Anagrafica")
    public CompanyData getCompany() {
        return this.company;
    }

    /**
     * @return the professionalRegister
     */
    @XmlElement(name = "AlboProfessionale")
    public String getProfessionalRegister() {
        return this.professionalRegister;
    }

    /**
     * @return the registerProvince
     */
    @XmlElement(name = "ProvinciaAlbo")
    public String getRegisterProvince() {
        return this.registerProvince;
    }

    /**
     * @return the registerRegNumber
     */
    @XmlElement(name = "NumeroIscrizioneAlbo")
    public String getRegisterRegNumber() {
        return this.registerRegNumber;
    }

    /**
     * @return the registerRegDate
     */
    @XmlElement(name = "DataIscrizioneAlbo")
    public String getRegisterRegDate() {
        return this.registerRegDate;
    }

    /**
     * @return the taxRegime
     */
    @XmlElement(name = "RegimeFiscale")
    public String getTaxRegime() {
        return this.taxRegime;
    }

    /**
     * 
     * @author Aliseo-G
     *
     */
    public static class PersonalDataBuilder {

        /**
         * 
         */
        private transient FiscalId fiscalId;

        /**
         * 
         */
        private transient String fiscalCode;

        /**
         * 
         */
        private final transient CompanyData company;

        /**
         * 
         */
        private transient String professionalRegister;

        /**
         * 
         */
        private transient String registerProvince;

        /**
         * 
         */
        private transient String registerRegNumber;

        /**
         * 
         */
        private transient String registerRegDate;

        /**
         * 
         */
        private transient String taxRegime;

        /**
         * @return the fiscalId
         */
        private FiscalId getFiscalId() {
            return this.fiscalId;
        }

        /**
         * @return the fiscalCode
         */
        private String getFiscalCode() {
            return this.fiscalCode;
        }

        /**
         * @return the company
         */
        private CompanyData getCompany() {
            return this.company;
        }

        /**
         * @return the professionalRegister
         */
        private String getProfessionalRegister() {
            return this.professionalRegister;
        }

        /**
         * @return the registerProvince
         */
        private String getRegisterProvince() {
            return this.registerProvince;
        }

        /**
         * @return the registerRegNumber
         */
        private String getRegisterRegNumber() {
            return this.registerRegNumber;
        }

        /**
         * @return the registerRegDate
         */
        private String getRegisterRegDate() {
            return this.registerRegDate;
        }

        /**
         * @return the taxRegime
         */
        private String getTaxRegime() {
            return this.taxRegime;
        }

        /**
         * 
         * @param fiscId
         *            a {@link FisclId} instance
         * @param com
         *            c {@link CompanyData} instance
         * @param taxReg
         *            the tax regime
         */
        public PersonalDataBuilder(final FiscalId fiscId, final CompanyData com,
                final String taxReg) {
            super();
            this.fiscalId = fiscId;
            this.company = com;
            this.taxRegime = taxReg;
        }

        /**
         * 
         * @param fiscId
         *            a {@link FisclId} instance
         * @param com 
         *            a {@link CompanyData} object
         */
        public PersonalDataBuilder(final FiscalId fiscId,
                final CompanyData com) {
            super();
            this.fiscalId = fiscId;
            this.company = com;
        }
        
        /**
         * 
         * @param code
         *            customer fiscla code
         * @param com 
         *            a {@link CompanyData} object
         */
        public PersonalDataBuilder(final String code,
                final CompanyData com) {
            super();
            this.fiscalCode = code;
            this.company = com;
        }
        
        public final PersonalDataBuilder withFiscalCode(final String code) {
            this.fiscalCode = code;
            return this;
        }
        
        /**
         * @param register
         *            the professionalRegister to set
         * @return the {@link PersonalDataBuilder} instance
         */
        public final PersonalDataBuilder withProfessionalRegister(
                final String register) {
            this.professionalRegister = register;
            return this;
        }

        /**
         * @param registerProv
         *            the registerProvince to with
         * @return the {@link PersonalDataBuilder} instance
         */
        public final PersonalDataBuilder withRegisterProvince(
                final String registerProv) {
            this.registerProvince = registerProv;
            return this;
        }

        /**
         * @param number
         *            the registerRegNumber to set
         * @return the {@link PersonalDataBuilder} instance
         */
        public final PersonalDataBuilder withRegisterRegNumber(
                final String number) {
            this.registerRegNumber = number;
            return this;
        }

        /**
         * @param date
         *            the registerRegDate to set
         * @return the {@link PersonalDataBuilder} instance
         */
        public final PersonalDataBuilder withRegisterRegDate(
                final String date) {
            this.registerRegDate = date;
            return this;
        }

        /**
         * 
         * Builds {@link PersonalData}.
         * 
         * @return the {@link PersonalData} instance
         */
        public final PersonalData build() {
            return new PersonalData(this);
        }

    }
}
