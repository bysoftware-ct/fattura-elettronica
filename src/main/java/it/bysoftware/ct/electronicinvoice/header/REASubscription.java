/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.header;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import it.bysoftware.ct.electronicinvoice.utils.Constants;
import it.bysoftware.ct.electronicinvoice.utils.WindUpState;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = { "office", "number", "shareCapital", "soleShareholder",
        "windUpState" })
public final class REASubscription {

    /**
     * 
     */
    private final transient DecimalFormat nFormatter;

    /**
     * 
     */
    private transient String office;

    /**
     * 
     */
    private transient String number;

    /**
     * 
     */
    private transient double shareCapital;

    /**
     * 
     */
    private transient String soleShareholder;

    /**
     * 
     */
    private transient String windUpState;

    /**
     * 
     */
    private REASubscription() {
        super();
        this.nFormatter = new DecimalFormat(Constants.DOUBLE_PATTERN,
                DecimalFormatSymbols.getInstance(Locale.US));
    }

    /**
     * @param builder
     *            the builder
     */
    protected REASubscription(final REASubscriptionBuilder builder) {
        this();
        this.office = builder.getOffice();
        this.number = builder.getNumber();
        this.shareCapital = builder.getShareCapital();
        this.soleShareholder = builder.getSoleShareholder();
        this.windUpState = builder.getWindUpState();
    }

    /**
     * @return the office
     */
    @XmlElement(name = "Ufficio")
    public String getOffice() {
        return this.office;
    }

    /**
     * @return the number
     */
    @XmlElement(name = "NumeroREA")
    public String getNumber() {
        return this.number;
    }

    /**
     * @return the shareCapital
     */
    @XmlElement(name = "CapitaleSociale")
    public String getShareCapital() {
        return this.nFormatter.format(this.shareCapital);
    }

    /**
     * @return the soleShareholder
     */
    @XmlElement(name = "SocioUnico")
    public String getSoleShareholder() {
        return this.soleShareholder;
    }

    /**
     * @return the windUpState
     */
    @XmlElement(name = "StatoLiquidazione")
    public String getWindUpState() {
        return this.windUpState;
    }

    /**
     * @author Aliseo-G
     *
     */
    public static class REASubscriptionBuilder {

        /**
         * 
         */
        private final transient String office;

        /**
         * 
         */
        private final transient String number;

        /**
         * 
         */
        private transient double shareCapital;

        /**
         * 
         */
        private transient String soleShareholder;

        /**
         * 
         */
        private final transient String windUpState;

        /**
         * @return the office
         */
        private String getOffice() {
            return this.office;
        }

        /**
         * @return the number
         */
        private String getNumber() {
            return this.number;
        }

        /**
         * @return the shareCapital
         */
        private double getShareCapital() {
            return this.shareCapital;
        }

        /**
         * @return the soleShareholder
         */
        private String getSoleShareholder() {
            return this.soleShareholder;
        }

        /**
         * @return the windUpState
         */
        private String getWindUpState() {
            return this.windUpState;
        }

        /**
         * @param officeName
         *            the office
         * @param num
         *            the number
         * @param wus
         *            the wind up state
         */
        public REASubscriptionBuilder(final String officeName,
                final String num, final WindUpState wus) {
            super();
            this.office = officeName;
            this.number = num;
            this.windUpState = wus.getVal();
        }

        /**
         * @param capital
         *            the shareCapital to set
         * @return the {@link REASubscriptionBuilder} object
         */
        public final REASubscriptionBuilder withShareCapital(
                final double capital) {
            this.shareCapital = capital;
            return this;
        }

        /**
         * @param ownership
         *            the soleShareholder to set
         * @return the {@link REASubscriptionBuilder} object
         */
        public final REASubscriptionBuilder withSoleShareholder(
                final String ownership) {
            this.soleShareholder = ownership;
            return this;
        }

        /**
         * Builds the {@link REASubscription} object.
         * 
         * @return the {@link REASubscription} object
         */
        public final REASubscription build() {
            return new REASubscription(this);
        }

    }

}
