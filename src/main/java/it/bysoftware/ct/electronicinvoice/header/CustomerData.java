/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.header;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = { "fiscalAgent" })
public final class CustomerData extends SubjectData {

    /**
     * 
     */
    private CustomerFiscalAgent fiscalAgent;

    /**
     * 
     */
    private CustomerData() {
        super();
    }

    /**
     * @param builder
     *            the builder
     */
    private CustomerData(final CustomerDataBuilder builder) {
        super(builder);
        this.fiscalAgent = builder.fiscalAgent;
    }

    /**
     * @return the fiscalAgent
     */
    @XmlElement(name = "RappresentanteFiscale")
    public CustomerFiscalAgent getFiscalAgent() {
        return this.fiscalAgent;
    }


    /**
     * @author Aliseo-G
     *
     */
    public static class CustomerDataBuilder extends
            SubjectData.SubjectDataBuilder<CustomerDataBuilder> {

        /**
         * 
         */
        private CustomerFiscalAgent fiscalAgent;

        /**
         * Creates a {@link SubjectDataBuilder} with the mandatory attributes.
         * 
         * @param data
         *            the {@link PersonalData} instance
         * @param address
         *            an {@link Address} instance
         */
        public CustomerDataBuilder(final PersonalData data,
                final Address address) {
            super(data, address);
        }

        /**
         * @param agent
         *            the fiscalAgent to set
         * @return the {@link CustomerDataBuilder} instance
         */
        public final CustomerDataBuilder withFiscalAgent(
                final CustomerFiscalAgent agent) {
            this.fiscalAgent = agent;
            return this;
        }

        /**
         * Builds the {@link CustomerData}.
         * 
         * @return the {@link CustomerData}
         */
        public final CustomerData build() {
            return new CustomerData(this);
        }
    }
}
