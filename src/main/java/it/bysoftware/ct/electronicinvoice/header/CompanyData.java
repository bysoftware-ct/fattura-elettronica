/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.header;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author Aliseo-G
 *
 */
public final class CompanyData {
    
    /**
     * 
     */
    private transient String businessName;
    
    /**
     * 
     */
    private transient String name;
    
    /**
     * 
     */
    private transient String surname;
    
    /**
     * 
     */
    private transient String title;
    
    /**
     * 
     */
    private transient String eoriCod;
    
    /**
     * 
     */
    private CompanyData() {
        super();
    }
    
    /**
     * 
     * @param n legal representative name
     * @param s legal representative surname
     */
    public CompanyData(final String n, final String s) {
        this();
        this.name = n;
        this.surname = s;
    }

    /**
     * 
     * @param n business name
     */
    public CompanyData(final String n) {
        this();
        this.businessName = n;
    }
    
    /**
     * @return the businessName
     */
    @XmlElement(name = "Denominazione")
    public String getBusinessName() {
        return this.businessName;
    }
    
    /**
     * @param n the businessName to set
     */
    public void setBusinessName(final String n) {
        this.businessName = n;
    }
    
    /**
     * @return the name
     */
    @XmlElement(name = "Nome")
    public String getName() {
        return this.name;
    }
    
    /**
     * @param n the name to set
     */
    public void setName(final String n) {
        this.name = n;
    }
    
    /**
     * @return the surname
     */
    @XmlElement(name = "Cognome")
    public String getSurname() {
        return this.surname;
    }
    
    /**
     * @param s the surname to set
     */
    public void setSurname(final String s) {
        this.surname = s;
    }
    
    /**
     * @return the title
     */
    @XmlElement(name = "Titolo")
    public String getTitle() {
        return this.title;
    }
    
    /**
     * @param t the title to set
     */
    public void setTitle(final String t) {
        this.title = t;
    }
    
    /**
     * @return the eORICod
     */
    @XmlElement(name = "CodEORI")
    public String getEORICod() {
        return this.eoriCod;
    }
    
    /**
     * @param eORICod the eORICod to set
     */
    public void setEORICod(final String eORICod) {
        eoriCod = eORICod;
    }
    
}
