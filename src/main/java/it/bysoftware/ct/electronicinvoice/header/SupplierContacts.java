/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.header;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author Aliseo-G
 *
 */
class SupplierContacts extends Contacts {

    /**
     * 
     */
    private String fax;

    /**
     * @return the fax
     */
    protected String getFax() {
        return fax;
    }

    /**
     * @param f the fax to set
     */
    @XmlElement(name = "Fax")
    protected void setFax(final String f) {
        this.fax = f;
    }
}
