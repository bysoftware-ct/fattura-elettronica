/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.header;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author Aliseo-G
 *
 */
public class FiscalId {

    /**
     * 
     */
    private String countryId;

    /**
     * 
     */
    private String senderCode;

    /**
     * 
     */
    private FiscalId() {
        super();
    }

    /**
     * 
     * @param id
     *            the Country id
     * @param code
     *            the fiscal code
     */
    public FiscalId(final String id, final String code) {
        this();
        this.countryId = id;
        this.senderCode = code;
    }

    /**
     * @return the countryId
     */
    @XmlElement(name = "IdPaese")
    public final String getCountryId() {
        return countryId;
    }

    /**
     * @param id
     *            the countryId to set
     */
    public final void setCountryId(final String id) {
        this.countryId = id;
    }

    /**
     * @return the senderCode
     */
    @XmlElement(name = "IdCodice")
    public final String getSenderCode() {
        return senderCode;
    }

    /**
     * @param code
     *            the senderCode to set
     */
    public final void setSenderCode(final String code) {
        this.senderCode = code;
    }

}
