/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.header;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = {"reaSubscription", "contact",
    "administrativeReference"})
public final class ProviderData extends SubjectData {

    /**
     * 
     */
    private transient REASubscription reaSubscription;

    /**
     * 
     */
    private transient SupplierContacts contact;

    /**
     * 
     */
    private transient String administrativeReference;

    /**
     * 
     */
    protected ProviderData() {
        super();
    }

    /**
     * @param builder
     *            the builder
     */
    protected ProviderData(final ProviderDataBuilder builder) {
        super(builder);
        this.reaSubscription = builder.getReaSubscription();
        this.contact = builder.getContact();
        this.administrativeReference = builder.getAdministrativeReference();
    }

    /**
     * @return the reaSubscription
     */
    @XmlElement(name = "IscrizioneREA")
    public REASubscription getReaSubscription() {
        return reaSubscription;
    }

    /**
     * @return the contact
     */
    @XmlElement(name = "Contatti")
    public SupplierContacts getContact() {
        return contact;
    }

    /**
     * @return the administrativeReference
     */
    @XmlElement(name = "RiferimentoAmministrazione")
    public String getAdministrativeReference() {
        return administrativeReference;
    }

    /**
     * @author Aliseo-G
     *
     */
    public static final class ProviderDataBuilder extends
            SubjectData.SubjectDataBuilder<ProviderDataBuilder> {

        /**
         * 
         */
        private transient REASubscription reaSubscription;

        /**
         * 
         */
        private transient SupplierContacts contact;

        /**
         * 
         */
        private transient String administrativeReference;

        /**
         * @return the reaSubscription
         */
        private REASubscription getReaSubscription() {
            return this.reaSubscription;
        }

        /**
         * @return the contact
         */
        private SupplierContacts getContact() {
            return this.contact;
        }

        /**
         * @return the administrativeReference
         */
        private String getAdministrativeReference() {
            return this.administrativeReference;
        }

        /**
         * @param data
         *            personal data
         * @param hAddress
         *            headquarters address
         */
        public ProviderDataBuilder(final PersonalData data,
                final Address hAddress) {
            super(data, hAddress);
        }

        /**
         * @param subscription
         *            the reaSubscription to set
         * @return {@link ProviderDataBuilder}
         */
        public ProviderDataBuilder withReaSubscription(
                final REASubscription subscription) {
            this.reaSubscription = subscription;
            return this;
        }

        /**
         * @param c
         *            the contact to set
         * @return {@link ProviderDataBuilder}
         */
        public ProviderDataBuilder withContact(final SupplierContacts c) {
            this.contact = c;
            return this;
        }

        /**
         * @param reference
         *            the administrativeReference to set
         * @return {@link ProviderDataBuilder}
         */
        public ProviderDataBuilder withAdministrativeReference(
                final String reference) {
            this.administrativeReference = reference;
            return this;
        }

        /**
         * Builds the provider data.
         * 
         * @return the {@link ProviderData} instance
         */
        public ProviderData build() {
            return new ProviderData(this);
        }

    }

}
