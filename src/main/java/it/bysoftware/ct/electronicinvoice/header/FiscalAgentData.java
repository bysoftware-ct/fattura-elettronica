/**
 *
 */
package it.bysoftware.ct.electronicinvoice.header;

/**
 * @author Aliseo-G
 *
 */
public final class FiscalAgentData extends SubjectData {

    /**
     *
     */
    private FiscalAgentData() {
        super();
    }

    /**
     * @param builder the {@link FiscalAgentDataBuilder} object
     */
    private FiscalAgentData(final FiscalAgentDataBuilder builder) {
        super(builder);
    }

    /**
     * @author Aliseo-G
     *
     */
    public static final class FiscalAgentDataBuilder extends
            SubjectData.SubjectDataBuilder<FiscalAgentDataBuilder> {

        /**
         *
         * @param data the {@link PersonalData}
         */
        public FiscalAgentDataBuilder(final PersonalData data) {
            super(data);
        }

        /**
         * Builds the {@link FiscalAgentData} object.
         *
         * @return the new {@link FiscalAgentData} object
         */
        public FiscalAgentData build() {
            return new FiscalAgentData(this);
        }

    }
}
