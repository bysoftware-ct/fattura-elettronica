/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.header;

import it.bysoftware.ct.electronicinvoice.utils.ElectronicInvoiceVersion;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = { "senderId", "number", "format", "recipientCode",
        "senderContacts", "certifiedEmail" })
public final class TransmissionData {

    /**
     * 
     */
    private transient FiscalId senderId;

    /**
     * 
     */
    private transient String number;

    /**
     * 
     */
    private transient ElectronicInvoiceVersion format;

    /**
     * 
     */
    private transient String recipientCode;

    /**
     * 
     */
    private transient SenderContacts senderContacts;

    /**
     * 
     */
    private transient String certifiedEmail;

    /**
     * 
     */
    private TransmissionData() {
        super();
    }

    /**
     * @param builder
     *            the {@link TransmissionDataBuilder} object
     */
    protected TransmissionData(final TransmissionDataBuilder builder) {
        this();
        this.senderId = builder.getSenderId();
        this.number = builder.getNumber();
        this.format = builder.getFormat();
        this.recipientCode = builder.getRecipientCode();
        this.senderContacts = builder.getSenderContacts();
        this.certifiedEmail = builder.getCertifiedEmail();
    }

    /**
     * @return the senderId
     */
    @XmlElement(name = "IdTrasmittente")
    public FiscalId getSenderId() {
        return senderId;
    }

    /**
     * Transmission number.
     * 
     * @return the number
     */
    @XmlElement(name = "ProgressivoInvio")
    public String getNumber() {
        return this.number;
    }

    /**
     * @return the format
     */
    @XmlElement(name = "FormatoTrasmissione")
    public String getFormat() {
        return this.format.getVal();
    }

    /**
     * Returns a {@link ElectronicInvoiceVersion} object.
     * @return {@link ElectronicInvoiceVersion} object
     */
    public ElectronicInvoiceVersion getFormatObj() {
        return this.format;
    }
    
    /**
     * @return the recipientCode
     */
    @XmlElement(name = "CodiceDestinatario")
    public String getRecipientCode() {
        return this.recipientCode;
    }

    /**
     * @return the senderContacts
     */
    @XmlElement(name = "ContattiTrasmittente")
    public SenderContacts getSenderContacts() {
        return this.senderContacts;
    }

    /**
     * @return the certifiedEmail
     */
    @XmlElement(name = "PECDestinatario")
    public String getCertifiedEmail() {
        return this.certifiedEmail;
    }

    /**
     * @author Aliseo-G
     *
     */
    public static class TransmissionDataBuilder {

        /**
         * 
         */
        private final transient FiscalId senderId;

        /**
         * 
         */
        private final transient String number;

        /**
         * 
         */
        private final transient ElectronicInvoiceVersion format;

        /**
         * 
         */
        private final transient String recipientCode;

        /**
         * 
         */
        private transient SenderContacts senderContacts;

        /**
         * 
         */
        private transient String certifiedEmail;

        /**
         * @return the senderId
         */
        private FiscalId getSenderId() {
            return senderId;
        }

        /**
         * @return the number
         */
        private String getNumber() {
            return number;
        }

        /**
         * @return the format
         */
        private ElectronicInvoiceVersion getFormat() {
            return format;
        }

        /**
         * @return the recipientCode
         */
        private String getRecipientCode() {
            return recipientCode;
        }

        /**
         * @return the senderContacts
         */
        private SenderContacts getSenderContacts() {
            return senderContacts;
        }

        /**
         * @return the certifiedEmail
         */
        private String getCertifiedEmail() {
            return certifiedEmail;
        }

        /**
         * @param fiscalId the {@link FiscalId} object
         * @param num the transmission number
         * @param f the format
         * @param code the recipient code
         */
        public TransmissionDataBuilder(final FiscalId fiscalId,
                final String num, final ElectronicInvoiceVersion f, final String code) {
            super();
            this.senderId = fiscalId;
            this.number = num;
            this.format = f;
            this.recipientCode = code;
        }

        /**
         * @param contacts
         *            the senderContacts to set
         * @return the {@link TransmissionDataBuilder} object
         */
        public final TransmissionDataBuilder withSenderContacts(
                final SenderContacts contacts) {
            this.senderContacts = contacts;
            return this;
        }

        /**
         * @param email
         *            the certifiedEmail to set
         * @return  the {@link TransmissionDataBuilder} object
         */
        public final TransmissionDataBuilder withCertifiedEmail(
                final String email) {
            this.certifiedEmail = email;
            return this;
        }

        /**
         * 
         * Builds a {@link TransmissionData} object.
         * 
         * @return the {@link TransmissionData} object
         */
        public final TransmissionData build() {
            return new TransmissionData(this);
        }

    }

}
