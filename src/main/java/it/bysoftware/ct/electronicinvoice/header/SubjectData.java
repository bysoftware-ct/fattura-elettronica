/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.header;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = { "personalData", "headQuaterAddress",
        "organizationAddress" })
public class SubjectData {

    /**
     * 
     */
    private transient PersonalData personalData;

    /**
     * 
     */
    private transient Address headQuaterAddress;

    /**
     * 
     */
    private transient Address organizationAddress;

    /**
     * 
     */
    protected SubjectData() {
        super();
    }

    /**
     * @param builder
     *            the builder
     */
    protected SubjectData(final SubjectDataBuilder<?> builder) {
        this();
        this.personalData = builder.getPersonalData();
        this.headQuaterAddress = builder.getHeadQuaterAddress();
        this.organizationAddress = builder.getOrganizationAddress();
    }

    /**
     * @return the personalData
     */
    @XmlElement(name = "DatiAnagrafici")
    public final PersonalData getPersonalData() {
        return personalData;
    }

    /**
     * @return the headQuaterAddress
     */
    @XmlElement(name = "Sede")
    public final Address getHeadQuaterAddress() {
        return headQuaterAddress;
    }

    /**
     * @return the organizationAddress
     */
    @XmlElement(name = "StabileOrganizzazione")
    public final Address getOrganizationAddress() {
        return organizationAddress;
    }

    /**
     * @author Aliseo-G
     * @param <T>
     *
     */
    public static class SubjectDataBuilder<T extends SubjectDataBuilder<T>> {

        /**
         * 
         */
        public final transient PersonalData personalData;

        /**
         * 
         */
        private transient Address headQuaterAddress;

        /**
         * 
         */
        private transient Address organizationAddress;

        /**
         * @return the personalData
         */
        private PersonalData getPersonalData() {
            return this.personalData;
        }

        /**
         * @return the headQuaterAddress
         */
        private Address getHeadQuaterAddress() {
            return this.headQuaterAddress;
        }

        /**
         * @return the organizationAddress
         */
        private Address getOrganizationAddress() {
            return this.organizationAddress;
        }

        /**
         * Creates a {@link SubjectDataBuilder} with the mandatory attributes.
         * 
         * @param data
         *            the {@link PersonalData} instance
         */
        public SubjectDataBuilder(final PersonalData data) {
            super();
            this.personalData = data;
        }

        /**
         * Creates a {@link SubjectDataBuilder} with the mandatory attributes.
         * 
         * @param data
         *            the {@link PersonalData} instance
         * @param address
         *            an {@link Address} instance
         */
        public SubjectDataBuilder(final PersonalData data,
                final Address address) {
            super();
            this.personalData = data;
            this.headQuaterAddress = address;
        }

        /**
         * @param address
         *            the organizationAddress to set
         * @return the object
         */
        @SuppressWarnings("unchecked")
        public final T withOrganizationAddress(final Address address) {
            this.organizationAddress = address;
            return (T) this;
        }

    }

}
