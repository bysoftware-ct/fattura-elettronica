/**
 *
 */
package it.bysoftware.ct.electronicinvoice.header;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class represents an address.
 *
 * @author Mario Torrisi
 *
 */
@XmlType(propOrder = {"address", "streetNumber", "postalCode", "town",
    "province", "country"})
public final class Address {

    /**
     * The actual address.
     */
    private transient String addressStr;

    /**
     * The street number.
     */
    private transient String streetNumber;

    /**
     * Postal code.
     */
    private transient String postalCode;

    /**
     * Town.
     */
    private transient String town;

    /**
     * Province. This must be two character long.
     */
    private transient String province;

    /**
     * Country. Must agree ISO 3166-1 alpha-2 code standard
     */
    private transient String country;

    /**
     * This class cannot be directly instantiated, please refer to
     * {@link AddressBuilder}.
     */
    private Address() {
        super();
    }

    /**
     * This class cannot be directly instantiated, please refer to
     * {@link AddressBuilder}.
     *
     * @param builder the builder
     */
    protected Address(final AddressBuilder builder) {
        this();
        this.addressStr = builder.getAddress();
        this.streetNumber = builder.getStreetNumber();
        this.postalCode = builder.getPostalCode();
        this.town = builder.getTown();
        this.province = builder.getProvince();
        this.country = builder.getCountry();
    }

    /**
     * Returns the address.
     *
     * @return the address
     */
    @XmlElement(name = "Indirizzo")
    public String getAddress() {
        return this.addressStr;
    }

    /**
     * Returns the street number.
     *
     * @return the street number
     */
    @XmlElement(name = "NumeroCivico")
    public String getStreetNumber() {
        return this.streetNumber;
    }

    /**
     * Returns the postal code.
     *
     * @return the postal code
     */
    @XmlElement(name = "CAP")
    public String getPostalCode() {
        return this.postalCode;
    }

    /**
     * Returns the town.
     *
     * @return the town
     */
    @XmlElement(name = "Comune")
    public String getTown() {
        return this.town;
    }

    /**
     * Returns the province.
     *
     * @return the province
     */
    @XmlElement(name = "Provincia")
    public String getProvince() {
        return this.province;
    }

    /**
     * Returns the country code.
     *
     * @return the country
     */
    @XmlElement(name = "Nazione")
    public String getCountry() {
        return this.country;
    }

    /**
     * This class allows to build an {@link Address}.
     *
     * @author Mario Torrisi
     *
     */
    public static final class AddressBuilder {

        /**
         * The address.
         */
        private final transient String address;

        /**
         * Street number.
         */
        private transient String streetNumber;

        /**
         * Postal code.
         */
        private final transient String postalCode;

        /**
         * Town.
         */
        private final transient String town;

        /**
         * Province. Must be two letters long.
         */
        private transient String province;

        /**
         * Country. Must agree ISO 3166-1 alpha-2 code standard.
         */
        private final transient String country;

        /**
         * Creates an {@link AddressBuilder} object with the mandatory fields.
         *
         * @param addr the address
         * @param pCode the postal code
         * @param twn the town
         * @param countryCode the country
         */
        public AddressBuilder(final String addr, final String pCode,
                final String twn, final String countryCode) {
            super();
            this.address = addr;
            this.postalCode = pCode;
            this.town = twn;
            this.country = countryCode;
        }

        /**
         * Builds the actual {@link Address} object with the specified fields
         * (optional and mandatory).
         *
         * @return build the Address
         */
        public Address build() {
            return new Address(this);
        }

        /**
         *
         * @param sNumber the streetNumber to set
         * @return the {@link AddressBuilder} instance
         */
        public AddressBuilder withStreetNumber(final String sNumber) {
            this.streetNumber = sNumber;
            return this;
        }

        /**
         * Sets the province in the builder.
         *
         * @param prov the province
         * @return the {@link AddressBuilder} instance
         */
        public AddressBuilder withProvince(final String prov) {
            this.province = prov;
            return this;
        }

        /**
         * Returns the address.
         *
         * @return the address
         */
        private String getAddress() {
            return this.address;
        }

        /**
         * Returns the street number.
         *
         * @return the streetNumber
         */
        private String getStreetNumber() {
            return this.streetNumber;
        }

        /**
         * Returns the postal code.
         *
         * @return the postalCode
         */
        private String getPostalCode() {
            return this.postalCode;
        }

        /**
         * Returns the town.
         *
         * @return the town
         */
        private String getTown() {
            return this.town;
        }

        /**
         * Returns the province.
         *
         * @return the province
         */
        private String getProvince() {
            return this.province;
        }

        /**
         * Returns the country.
         *
         * @return the country
         */
        private String getCountry() {
            return this.country;
        }

    }

}
