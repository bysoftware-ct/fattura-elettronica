/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.header;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = { "fiscalId", "businessName", "name", "surname" })
public final class CustomerFiscalAgent {

    /**
     * 
     */
    private transient FiscalId fiscalId;

    /**
     * 
     */
    private transient String businessName;

    /**
     * 
     */
    private transient String name;

    /**
     * 
     */
    private transient String surname;

    /**
     * 
     */
    private CustomerFiscalAgent() {
        super();
    }

    /**
     * @param builder
     *            the {@link CustomerFiscalAgentDataBuilder} object
     */
    protected CustomerFiscalAgent(
            final CustomerFiscalAgentDataBuilder builder) {
        this();
        this.fiscalId = builder.getFiscalId();
        this.businessName = builder.getBusinessName();
        this.name = builder.getName();
        this.surname = builder.getSurname();
    }

    /**
     * @return the fiscalId
     */
    @XmlElement(name = "IdFiscaleIVA")
    public FiscalId getFiscalId() {
        return this.fiscalId;
    }

    /**
     * @return the businessName
     */
    @XmlElement(name = "Denominazione")
    public String getBusinessName() {
        return this.businessName;
    }

    /**
     * @return the name
     */
    @XmlElement(name = "Name")
    public String getName() {
        return this.name;
    }

    /**
     * @return the surname
     */
    @XmlElement(name = "Cognome")
    public String getSurname() {
        return this.surname;
    }

    /**
     * @author Aliseo-G
     *
     */
    public static final class CustomerFiscalAgentDataBuilder {

        /**
         * 
         */
        private final transient FiscalId fiscalId;

        /**
         * 
         */
        private transient String businessName;

        /**
         * 
         */
        private transient String name;

        /**
         * 
         */
        private transient String surname;

        /**
         * 
         * @param id
         *            the {@link FiscalId}
         */
        public CustomerFiscalAgentDataBuilder(final FiscalId id) {
            this.fiscalId = id;
        }

        /**
         * @param company
         *            the businessName to set
         * @return {@link CustomerFiscalAgentDataBuilder} with business name
         *         set.
         */
        public CustomerFiscalAgentDataBuilder withBusinessName(
                final String company) {
            this.businessName = company;
            return this;
        }

        /**
         * @param fName
         *            the name to set
         * @param fSurname
         *            the surname to set
         * @return {@link CustomerFiscalAgentDataBuilder} with name and surname
         *         set
         */
        public CustomerFiscalAgentDataBuilder withNameSurname(
                final String fName, final String fSurname) {
            this.name = fName;
            this.name = fSurname;
            return this;
        }

        /**
         * Builds the {@link CustomerFiscalAgent} object.
         * 
         * @return the new {@link CustomerFiscalAgent} object
         */
        public CustomerFiscalAgent build() {
            return new CustomerFiscalAgent(this);
        }

        /**
         * @return the fiscalId
         */
        private FiscalId getFiscalId() {
            return this.fiscalId;
        }

        /**
         * @return the businessName
         */
        private String getBusinessName() {
            return this.businessName;
        }

        /**
         * @return the name
         */
        private String getName() {
            return this.name;
        }

        /**
         * @return the surname
         */
        private String getSurname() {
            return this.surname;
        }

    }
}
