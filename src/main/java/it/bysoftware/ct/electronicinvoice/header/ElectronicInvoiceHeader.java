/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.header;

import it.bysoftware.ct.electronicinvoice.utils.IssuerType;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = { "transmissionData", "provider", "fiscalAgent",
        "customer", "issuer", "issuerType" })
public final class ElectronicInvoiceHeader {

    /**
     * 
     */
    private transient TransmissionData transmissionData;

    /**
     * 
     */
    private transient SubjectData provider;

    /**
     * 
     */
    private transient SubjectData fiscalAgent;

    /**
     * 
     */
    private transient SubjectData customer;

    /**
     * 
     */
    private transient SubjectData issuer;

    /**
     * 
     */
    private transient IssuerType issuerType;

    /**
     * 
     */
    private ElectronicInvoiceHeader() {
        super();
    }

    /**
     * @param builder
     *            the {@link ElectronicInvoiceHeaderBuilder} instance
     */
    protected ElectronicInvoiceHeader(
            final ElectronicInvoiceHeaderBuilder builder) {
        this();
        this.transmissionData = builder.getTransmissionData();
        this.fiscalAgent = builder.getFiscalAgent();
        this.provider = builder.getProvider();
        this.customer = builder.getCustomer();
        this.issuer = builder.getIssuer();
        this.issuerType = builder.getIssuerType();
    }

    /**
     * @return the transmissionData
     */
    @XmlElement(name = "DatiTrasmissione")
    public TransmissionData getTransmissionData() {
        return this.transmissionData;
    }

    /**
     * @return the provider
     */
    @XmlElement(name = "CedentePrestatore", type = ProviderData.class)
    public SubjectData getProvider() {
        return this.provider;
    }

    /**
     * @return the fiscalAgent
     */
    @XmlElement(name = "RappresentanteFiscale", type = FiscalAgentData.class)
    public SubjectData getFiscalAgent() {
        return this.fiscalAgent;
    }

    /**
     * @return the provider
     */
    @XmlElement(name = "CessionarioCommittente", type = CustomerData.class)
    public SubjectData getCustomer() {
        return this.customer;
    }

    /**
     * @return the issuer
     */
    @XmlElement(name = "TerzoIntermediarioOSoggettoEmittente",
            type = IssuerData.class)
    public SubjectData getIssuer() {
        return this.issuer;
    }

    /**
     * @return the issuerType
     */
    @XmlElement(name = "SoggettoEmittente")
    public String getIssuerType() {
        String res;
        if (this.issuerType == null) {
            res = null;
        } else {
            res = this.issuerType.getVal();
        }
        return res;
    }

    /**
     * @author Aliseo-G
     *
     */
    public static final class ElectronicInvoiceHeaderBuilder {
        /**
         * 
         */
        private final transient TransmissionData transmissionData;

        /**
         * 
         */
        private transient SubjectData fiscalAgent;

        /**
         * 
         */
        private final transient SubjectData provider;

        /**
         * 
         */
        private final transient SubjectData customer;

        /**
         * 
         */
        private transient SubjectData issuer;

        /**
         * 
         */
        private transient IssuerType issuerType;

        /**
         * @return the transmissionData
         */
        private TransmissionData getTransmissionData() {
            return this.transmissionData;
        }

        /**
         * @return the fiscalAgent
         */
        private SubjectData getFiscalAgent() {
            return this.fiscalAgent;
        }

        /**
         * @return the provider
         */
        private SubjectData getProvider() {
            return this.provider;
        }

        /**
         * @return the customer
         */
        private SubjectData getCustomer() {
            return this.customer;
        }

        /**
         * @return the issuer
         */
        private SubjectData getIssuer() {
            return this.issuer;
        }

        /**
         * @return the issuerType
         */
        private IssuerType getIssuerType() {
            return this.issuerType;
        }

        /**
         * @param data
         *            the {@link TransmissionData} object
         * @param providerData
         *            the {@link ProviderData} object
         * @param customerData
         *            the {@link SubjectData} object
         */
        public ElectronicInvoiceHeaderBuilder(final TransmissionData data,
                final SubjectData providerData,
                final SubjectData customerData) {
            super();
            this.transmissionData = data;
            this.provider = providerData;
            this.customer = customerData;
        }

        /**
         * @param agent
         *            the fiscalAgent to set
         * @return the {@link ElectronicInvoiceHeaderBuilder} instance
         */
        public ElectronicInvoiceHeaderBuilder withFiscalAgent(
                final SubjectData agent) {
            this.fiscalAgent = agent;
            return this;
        }

        /**
         * @param iss
         *            the issuer to set
         * @return the {@link ElectronicInvoiceHeaderBuilder} instance
         */
        public ElectronicInvoiceHeaderBuilder withIssuer(
                final SubjectData iss) {
            this.issuer = iss;
            return this;
        }

        /**
         * @param type
         *            the issuerType to set
         * @return the {@link ElectronicInvoiceHeaderBuilder} instance
         */
        public ElectronicInvoiceHeaderBuilder withIssuerType(
                final IssuerType type) {
            this.issuerType = type;
            return this;
        }

        /**
         * Builds a new {@link ElectronicInvoiceHeader}.
         * 
         * @return the new {@link ElectronicInvoiceHeader}.
         */
        public ElectronicInvoiceHeader build() {
            return new ElectronicInvoiceHeader(this);
        }
    }

}
