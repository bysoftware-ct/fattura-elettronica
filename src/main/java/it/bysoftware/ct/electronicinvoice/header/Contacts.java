/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.header;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author Aliseo-G
 *
 */
public class Contacts {

    /**
     * 
     */
    private String telephone;

    /**
     * 
     */
    private String email;

    /**
     * @return the telephone
     */
    @XmlElement(name = "Telefono")
    protected final String getTelephone() {
        return this.telephone;
    }

    /**
     * @param t
     *            the telephone to set
     */
    protected final void setTelephone(final String t) {
        this.telephone = t;
    }

    /**
     * @return the email
     */
    @XmlElement(name = "Email")
    protected final String getEmail() {
        return this.email;
    }

    /**
     * @param e
     *            the email to set
     */
    protected final void setEmail(final String e) {
        this.email = e;
    }

}
