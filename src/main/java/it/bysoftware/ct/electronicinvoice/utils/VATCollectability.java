/**
 *
 */
package it.bysoftware.ct.electronicinvoice.utils;

/**
 * @author Aliseo-G
 *
 */
public enum VATCollectability {
    /**
     *
     */
    IMMEDIATE("I"),
    /**
     *
     */
    DEFERRED("D"),
    /**
     *
     */
    SUSPENSION("S");

    /**
     * Entry value.
     */
    private final String val;

    private VATCollectability(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }

}
