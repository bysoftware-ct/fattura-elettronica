/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Aliseo-G
 *
 */
public final class Utils {

    /**
     * This is an utility class, so it and cannot be instantiated.
     */
    private Utils() {
        super();
    }

    /**
     * Split text into n number of characters.
     * 
     * @param text
     *            the text to be split.
     * @param size
     *            the split size
     * @return an list of the split text.
     */
    public static List<String> splitToNChar(final String text, final int size) {
        final List<String> parts = new ArrayList<>();
        if (text != null) {
            final int length = text.length();
            for (int i = 0; i < length; i += size) {
                parts.add(text.substring(i, Math.min(length, i + size)));
            }
        }
        return parts;
    }

}
