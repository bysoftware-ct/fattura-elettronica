/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.utils;

/**
 * @author Aliseo-G
 *
 */
public enum LineType {

    /**
     * 
     */
    DISCOUNT("SC"),

    /**
     * 
     */
    BONUS("PR"),

    /**
     * 
     */
    REBATE("AB"),

    /**
     * 
     */
    ACCESSORY_COST("AC");

    /**
     * Entry value.
     */
    private final String val;

    /**
     * @param value the value
     */
    private LineType(final String value) {
        this.val = value;
    }

    /**
     * @return the val
     */
    public final String getVal() {
        return val;
    }

}
