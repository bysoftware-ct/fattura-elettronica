/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.utils;

/**
 * @author Aliseo-G
 *
 */
public enum WindUpState {
    
    /**
     * 
     */
    IN_WIND_UP("LS"),
    
    /**
     * 
     */
    NOT_IN_WIND_UP("LN");
    
    /**
     * Entry value.
     */
    private final String val;

    /**
     * @param value the value
     */
    WindUpState(final String value) {
        this.val = value;
    }

    /**
     * @return the value
     */
    public final String getVal() {
        return val;
    }
    
}
