/**
 *
 */
package it.bysoftware.ct.electronicinvoice.utils;

/**
 * @author Aliseo-G
 *
 */
public final class Constants {

    /**
     * Date format pattern.
     */
    public static final String DATE_PATTERN = "yyyy-MM-dd";
    
    /**
     * Timestamp format pattern.
     */
    public static final String TIMESTAMP_PATTERN = "yyyy-MM-ddThh:mm:ss";

    /**
     * Decimal format pattern.
     */
    public static final String DOUBLE_PATTERN = "0.00";

    /**
     * Decimal format pattern.
     */
    public static final String QUANTITY_PATTERN = "0.00000";

    /**
     * Decimal format pattern.
     */
    public static final String PRICE_PATTERN = "0.0000000";

    /**
     * Decimal format pattern.
     */
    public static final int PRIVATE_COMPANY_CODE_LENGTH = 7;
    
    /**
     * Decimal format pattern.
     */
    public static final int PUBLIC_ADMINISTRATION_CODE_LENGTH = 6;
    
    /**
     * This is an utility and cannot be instantiated.
     */
    private Constants() {
        super();
    }

}
