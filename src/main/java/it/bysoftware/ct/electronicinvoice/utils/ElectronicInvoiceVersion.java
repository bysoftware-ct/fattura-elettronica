/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.utils;

/**
 * @author Aliseo-G
 *
 */
public enum ElectronicInvoiceVersion {

    /**
     * Electronic invoice for Italian Public Administration.
     */
    PUBLIC_ADMINISTRATION("FPA12"),

    /**
     * Electronic invoice for a private company.
     */
    PRIVATE_COMPANY("FPR12");

    /**
     * stores version value.
     */
    private final String value;

    /**
     * This enum could not be instantiated.
     * 
     * @param val
     *            the value to set.
     */
    private ElectronicInvoiceVersion(final String val) {
        this.value = val;
    }

    /**
     * Returns the Electronic invoice version.
     * 
     * @return the Electronic invoice version
     */
    public final String getVal() {
        return value;
    }

}
