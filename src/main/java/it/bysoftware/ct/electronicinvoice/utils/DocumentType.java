package it.bysoftware.ct.electronicinvoice.utils;

/**
 * Represents the available type of documents.
 *
 * @author Mario Torrisi torrisi.mario@gmail.com
 */
public enum DocumentType {
    /**
     * Invoice.
     */
    INVOICE("TD01"),

    /**
     * Invoice down payment.
     */
    INVOICE_DOWN_PAYMENTS("TD02"),

    /**
     * Fee down payment.
     */
    FEE_DOWN_PAYMENTS("TD03"),

    /**
     * Credit note.
     */
    CREDIT_NOTE("TD04"),

    /**
     * Debt note.
     */
    DEBT_NOTE("TD05"),

    /**
     * Fee.
     */
    FEE("TD06");

    /**
     * Entry value.
     */
    private final String val;

    /**
     * @param value
     *            the value
     */
    private DocumentType(final String value) {
        this.val = value;
    }

    /**
     * @return the value
     */
    public final String getVal() {
        return val;
    }
}
