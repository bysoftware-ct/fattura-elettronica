/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.utils;

/**
 * @author Aliseo-G
 *
 */
public enum IssuerType {
    /**
     * 
     */
    CUSTOMER("CC"),

    /**
     * 
     */
    THIRD_SUBJECT("TZ");

    /**
     * Entry value.
     */
    private final String val;

    /**
     * @param v the value
     */
    private IssuerType(final String v) {
        this.val = v;
    }

    /**
     * @return the val
     */
    public final String getVal() {
        return val;
    }

}
