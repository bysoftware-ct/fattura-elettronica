/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.utils;

/**
 * @author Aliseo-G
 *
 */
public enum DiscountSurchargeType {
    /**
     * 
     */
    DISCOUNT("SC"),

    /**
     * 
     */
    SURCHARGE("MG");

    /**
     * Entry value.
     */
    private final String val;

    /**
     * @param v the value
     */
    private DiscountSurchargeType(final String v) {
        this.val = v;
    }

    /**
     * @return the val
     */
    public final String getVal() {
        return val;
    }

}
