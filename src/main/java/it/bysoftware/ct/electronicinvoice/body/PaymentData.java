/**
 *
 */
package it.bysoftware.ct.electronicinvoice.body;

import it.bysoftware.ct.electronicinvoice.body.goodsandservices.RowDetail;
import it.bysoftware.ct.electronicinvoice.body.paymentdetails.PaymentDetail;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = {"conditions", "details"})
public class PaymentData {

    /**
     * The document rows {@link RowDetail}.
     */
    private transient String conditions;

    /**
     * The document rows {@link RowDetail}.
     */
    private transient List<PaymentDetail> details;

    private PaymentData() {
        super();
    }

    public PaymentData(String conditions, List<PaymentDetail> details) {
        this.conditions = conditions;
        this.details = details;
    }

    @XmlElement(name = "CondizioniPagamento")
    public String getConditions() {
        return this.conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    @XmlElement(name = "DettaglioPagamento")
    public List<PaymentDetail> getDetails() {
        return this.details;
    }

    public void setDetails(List<PaymentDetail> details) {
        this.details = details;
    }

}
