/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.body;

import it.bysoftware.ct.electronicinvoice.body.generaldata.CarriageData;
import it.bysoftware.ct.electronicinvoice.body.generaldata.DocumentData;
import it.bysoftware.ct.electronicinvoice.body.generaldata.DocumentGeneralData;
import it.bysoftware.ct.electronicinvoice.body.generaldata.MainInvoice;
import it.bysoftware.ct.electronicinvoice.body.generaldata.TransportDocument;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = { "docData", "orderData", "contractData",
        "conventionData", "receiveData", "linkedInvoiceData", "references",
        "transDocuments", "carriageData", "mainInvoice" })
public final class GeneralData {

    /**
     * Document general data.
     */
    private transient DocumentGeneralData docData;

    /**
     * Order data.
     */
    private transient List<DocumentData> orderData;

    /**
     * Contract data.
     */
    private transient List<DocumentData> contractData;

    /**
     * Convention data.
     */
    private transient List<DocumentData> conventionData;

    /**
     * Receipt data.
     */
    private transient List<DocumentData> receiveData;

    /**
     * Linked invoice data.
     */
    private transient List<DocumentData> linkedInvoiceData;

    /**
     * Work progress status data.
     */
    private transient List<Integer> references;

    /**
     * Transport documents data.
     */
    private transient List<TransportDocument> transDocuments;

    /**
     * Carriage data.
     */
    private transient CarriageData carriageData;

    /**
     * Carriage data.
     */
    private transient MainInvoice mainInvoice;

    /**
     * This class cannot be directly instantiated, please refer to
     * {@link GeneralDataBuilder}.
     */
    private GeneralData() {
        super();
    }

    /**
     * This class cannot be directly instantiated, please refer to
     * {@link GeneralDataBuilder}.
     * 
     * @param builder
     *            the builder
     */
    protected GeneralData(final GeneralDataBuilder builder) {
        this();
        this.docData = builder.getDocData();
        this.orderData = builder.getOrderData();
        this.contractData = builder.getContractData();
        this.conventionData = builder.getConventionData();
        this.receiveData = builder.getReceiveData();
        this.linkedInvoiceData = builder.getLinkedInvoiceData();
        this.references = builder.getReferences();
        this.transDocuments = builder.getTransDocuments();
        this.carriageData = builder.getCarriageData();
        this.mainInvoice = builder.getMainInvoice();
    }

    /**
     * @return the docData
     */
    @XmlElement(name = "DatiGeneraliDocumento")
    public DocumentGeneralData getDocData() {
        return this.docData;
    }

    /**
     * @return the orderData
     */
    @XmlElement(name = "DatiOrdineAcquisto")
    public List<DocumentData> getOrderData() {
        return this.orderData;
    }

    /**
     * @return the contractData
     */
    @XmlElement(name = "DatiContratto")
    public List<DocumentData> getContractData() {
        return this.contractData;
    }

    /**
     * @return the conventionData
     */
    @XmlElement(name = "DatiConvenzione")
    public List<DocumentData> getConventionData() {
        return this.conventionData;
    }

    /**
     * @return the receiveData
     */
    @XmlElement(name = "DatiRicezione")
    public List<DocumentData> getReceiveData() {
        return this.receiveData;
    }

    /**
     * @return the linkedInvoiceData
     */
    @XmlElement(name = "DatiFattureCollegate")
    public List<DocumentData> getLinkedInvoiceData() {
        return this.linkedInvoiceData;
    }

    /**
     * @return the references
     */
    @XmlElement(name = "RiferimentoFase")
    @XmlElementWrapper(name = "DatiSAL")
    public List<Integer> getReferences() {
        return this.references;
    }

    /**
     * @return the transDocuments
     */
    @XmlElement(name = "DatiDDT")
    public List<TransportDocument> getTransDocuments() {
        return this.transDocuments;
    }

    /**
     * @return the carriageData
     */
    @XmlElement(name = "DatiTrasporto")
    public CarriageData getCarriageData() {
        return this.carriageData;
    }

    /**
     * @return the mainInvoice
     */
    @XmlElement(name = "FatturaPrincipale")
    public MainInvoice getMainInvoice() {
        return this.mainInvoice;
    }

    /**
     * @author Aliseo-G
     *
     */
    public static final class GeneralDataBuilder {

        /**
         * Document general data.
         */
        private final transient DocumentGeneralData docData;

        /**
         * Order data.
         */
        private transient List<DocumentData> orderData;

        /**
         * Contract data.
         */
        private transient List<DocumentData> contractData;

        /**
         * Convention data.
         */
        private transient List<DocumentData> conventionData;

        /**
         * Receipt data.
         */
        private transient List<DocumentData> receiveData;

        /**
         * Linked invoice data.
         */
        private transient List<DocumentData> linkedInvoiceData;

        /**
         * Work progress status data.
         */
        private transient List<Integer> references;

        /**
         * Transport documents data.
         */
        private transient List<TransportDocument> transDocuments;

        /**
         * Carriage data.
         */
        private transient CarriageData carriageData;

        /**
         * Carriage data.
         */
        private transient MainInvoice mainInvoice;

        /**
         * Creates a general data builder.
         * 
         * @param data
         *            the document general data.
         */
        public GeneralDataBuilder(final DocumentGeneralData data) {
            super();
            this.docData = data;
        }

        /**
         * Returns a {@link GeneralDataBuilder} with the order data to set.
         * 
         * @param data
         *            the order data to set
         * @return a {@link GeneralDataBuilder} with the order data to set.
         */
        public GeneralDataBuilder withOrderData(final List<DocumentData> data) {
            this.orderData = data;
            return this;
        }

        /**
         * Returns a {@link GeneralDataBuilder} with the contract data to set.
         * 
         * @param data
         *            the contract data to set
         * @return a {@link GeneralDataBuilder} with the contract data to set.
         */
        public GeneralDataBuilder withContractData(
                final List<DocumentData> data) {
            this.contractData = data;
            return this;
        }

        /**
         * Returns a {@link GeneralDataBuilder} with the convention data to set.
         * 
         * @param data
         *            the convention data to set
         * @return a {@link GeneralDataBuilder} with the convention data to set.
         */
        public GeneralDataBuilder withConventionData(
                final List<DocumentData> data) {
            this.conventionData = data;
            return this;
        }

        /**
         * Returns a {@link GeneralDataBuilder} with the receipt data to set.
         * 
         * @param data
         *            the receipt data to set
         * @return a {@link GeneralDataBuilder} with the receipt data to set.
         */
        public GeneralDataBuilder withReceiveData(final List<DocumentData> data)
        {
            this.receiveData = data;
            return this;
        }

        /**
         * Returns a {@link GeneralDataBuilder} with the linked invoices list to
         * set.
         * 
         * @param invoices
         *            the linked invoices list to set
         * @return a {@link GeneralDataBuilder} with the linked invoices list to
         *         set.
         */
        public GeneralDataBuilder withLinkedInvoiceData(
                final List<DocumentData> invoices) {
            this.linkedInvoiceData = invoices;
            return this;
        }

        /**
         * Returns a {@link GeneralDataBuilder} with the phases of work progress
         * list to set.
         * 
         * @param list
         *            the phases of work progress list to set
         * @return a {@link GeneralDataBuilder} with the phases of work progress
         *         list to set.
         */
        public GeneralDataBuilder withReferences(final List<Integer> list) {
            this.references = list;
            return this;
        }

        /**
         * Returns a {@link GeneralDataBuilder} with transport documents data
         * set.
         * 
         * @param data
         *            the transport documents data to set
         * @return a {@link GeneralDataBuilder} with transport documents data
         *         set.
         */
        public GeneralDataBuilder withTransDocuments(
                final List<TransportDocument> data) {
            this.transDocuments = data;
            return this;
        }

        /**
         * Returns a {@link GeneralDataBuilder} with carriage data set.
         * 
         * @param data
         *            the carriage data to set
         * @return a {@link GeneralDataBuilder} with carriage data set.
         */
        public GeneralDataBuilder withCarriageData(final CarriageData data) {
            this.carriageData = data;
            return this;
        }

        /**
         * Returns a {@link GeneralDataBuilder} with main invoice set.
         * 
         * @param invoice
         *            the mainInvoice to set
         * @return a {@link GeneralDataBuilder} with main invoice set.
         */
        public GeneralDataBuilder withMainInvoice(final MainInvoice invoice) {
            this.mainInvoice = invoice;
            return this;
        }

        /**
         * Builds a new {@link GeneralData} instance with the specified
         * attribute set.
         * 
         * @return new {@link GeneralData} instance with the specified attribute
         *         set.
         */
        public GeneralData build() {
            return new GeneralData(this);
        }

        /**
         * @return the docData
         */
        private DocumentGeneralData getDocData() {
            return this.docData;
        }

        /**
         * @return the orderData
         */
        private List<DocumentData> getOrderData() {
            return this.orderData;
        }

        /**
         * @return the contractData
         */
        private List<DocumentData> getContractData() {
            return this.contractData;
        }

        /**
         * @return the conventionData
         */
        private List<DocumentData> getConventionData() {
            return this.conventionData;
        }

        /**
         * @return the receiveData
         */
        private List<DocumentData> getReceiveData() {
            return this.receiveData;
        }

        /**
         * @return the linkedInvoiceData
         */
        private List<DocumentData> getLinkedInvoiceData() {
            return this.linkedInvoiceData;
        }

        /**
         * @return the references
         */
        private List<Integer> getReferences() {
            return this.references;
        }

        /**
         * @return the transDocuments
         */
        private List<TransportDocument> getTransDocuments() {
            return this.transDocuments;
        }

        /**
         * @return the carriageData
         */
        private CarriageData getCarriageData() {
            return this.carriageData;
        }

        /**
         * @return the mainInvoice
         */
        private MainInvoice getMainInvoice() {
            return this.mainInvoice;
        }

    }
}
