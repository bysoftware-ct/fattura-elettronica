/**
 *
 */
package it.bysoftware.ct.electronicinvoice.body.generaldata;

import it.bysoftware.ct.electronicinvoice.utils.Constants;
import it.bysoftware.ct.electronicinvoice.utils.DiscountSurchargeType;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = {"type", "percentage"})
public final class DiscountSurcharge {

    /**
     * Number formatter.
     */
    private final transient DecimalFormat nFormatter;

    /**
     * {@link DiscountSurcharge} type.
     */
    private transient DiscountSurchargeType type;

    /**
     * {@link DiscountSurcharge} percentage.
     */
    private transient double percentage;

    /**
     * Should be never invoked.
     */
    private DiscountSurcharge() {
        super();
        this.nFormatter = new DecimalFormat(Constants.DOUBLE_PATTERN,
                DecimalFormatSymbols.getInstance(Locale.US));
    }

    /**
     * Creates a {@link DiscountSurcharge} object.
     *
     * @param builder {@link DiscountSurchargeBuilder}
     */
    protected DiscountSurcharge(final DiscountSurchargeBuilder builder) {
        this();
        this.type = builder.getType();
        this.percentage = builder.getPercentage();
    }

    /**
     * Returns the {@link DiscountSurcharge} percentage.
     *
     * @return the percentage
     */
    @XmlElement(name = "Percentuale")
    public String getPercentage() {
        return this.nFormatter.format(Math.abs(this.percentage));
    }

    /**
     * Returns the {@link DiscountSurcharge} type.
     *
     * @return the type
     */
    @XmlElement(name = "Tipo")
    public String getType() {
        return this.type.getVal();
    }

    /**
     * This class allows to build an {@link DiscountSurcharge}.
     *
     * @author Mario Torrisi
     *
     */
    public static final class DiscountSurchargeBuilder {

        /**
         * {@link DiscountSurcharge} type.
         */
        private final transient DiscountSurchargeType type;

        /**
         * {@link DiscountSurcharge} percentage.
         */
        private transient double percentage;

        /**
         * Creates an {@link DiscountSurchargeBuilder} object with the mandatory
         * fields.
         *
         * @param t {@link DiscountSurchargeType}
         */
        public DiscountSurchargeBuilder(final DiscountSurchargeType t) {
            super();
            this.type = t;
        }

        /**
         * Builds the actual {@link DiscountSurcharge} object with the specified
         * fields (optional and mandatory).
         *
         * @return build the Address
         */
        public DiscountSurcharge build() {
            return new DiscountSurcharge(this);
        }

        /**
         * Sets discount or surcharge percentage.
         *
         * @param perc discount or surcharge percentage
         * @return the {@link DiscountSurchargeBuilder} instance
         */
        public DiscountSurchargeBuilder withPercentage(final double perc) {
            this.percentage = perc;
            return this;
        }

        /**
         * Returns the {@link DiscountSurchargeType}.
         *
         * @return the type
         */
        private DiscountSurchargeType getType() {
            return type;
        }

        /**
         * Returns the discount or surcharge percentage.
         *
         * @return the percentage
         */
        private double getPercentage() {
            return percentage;
        }

    }

}
