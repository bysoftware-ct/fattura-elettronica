/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.body.goodsandservices;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import it.bysoftware.ct.electronicinvoice.utils.Constants;
import it.bysoftware.ct.electronicinvoice.utils.VATCollectability;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = { "vatPercentage", "vatNature", "additionalCosts",
        "rounding", "taxable", "tax", "vatCollectability", "lawReference" })
public final class Summary {

    /**
     * VAT percentage.
     */
    private transient double vatPercentage;

    /**
     * VAT Nature.
     */
    private transient String vatNature;

    /**
     * Rounding.
     */
    private transient double rounding;

    /**
     * Taxable amount.
     */
    private transient double taxable;

    /**
     * Tax amount.
     */
    private transient double tax;

    /**
     * VAT collectability.
     */
    private transient VATCollectability vatCollectability;

    /**
     * Law reference.
     */
    private transient String lawReference;

    /**
     * Additional costs.
     */
    private transient double additionalCosts;

    /**
     * Number formatter.
     */
    private final transient DecimalFormat nFormatter;

    /**
     * 
     */
    private Summary() {
        super();
        this.nFormatter = new DecimalFormat(Constants.DOUBLE_PATTERN,
                DecimalFormatSymbols.getInstance(Locale.US));
    }

    /**
     * @param builder
     *            the {@link SummaryBuilder}
     */
    protected Summary(final SummaryBuilder builder) {
        this();
        this.vatPercentage = builder.getVatPercentage();
        this.vatNature = builder.getVatNature();
        this.additionalCosts = builder.getAdditionalCosts();
        this.rounding = builder.getRounding();
        this.taxable = builder.getTaxable();
        this.tax = builder.getTax();
        this.vatCollectability = builder.getVatCollectability();
        this.lawReference = builder.getLawReference();
    }

    /**
     * @return the vatPercentage
     */
    @XmlElement(name = "AliquotaIVA")
    public String getVatPercentage() {
        return this.nFormatter.format(this.vatPercentage);
    }

    /**
     * @return the vatNature
     */
    @XmlElement(name = "Natura")
    public String getVatNature() {
        return this.vatNature;
    }

    /**
     * @return the rounding
     */
    @XmlElement(name = "SpeseAccessorie")
    public String getAdditionalCosts() {
        String res;
        if (this.additionalCosts == 0) {
            res = null;            
        } else {
            res = this.nFormatter.format(this.additionalCosts);
        }
        return res;
    }

    /**
     * @return the rounding
     */
    @XmlElement(name = "Arrotondamento")
    public String getRounding() {
        String res;
        if (this.rounding == 0) {
            res = null;            
        } else {
            res = this.nFormatter.format(this.rounding);
        }
        return res;
    }

    /**
     * @return the taxable
     */
    @XmlElement(name = "ImponibileImporto")
    public String getTaxable() {
        return this.nFormatter.format(this.taxable);
    }

    /**
     * @return the taxable
     */
    @XmlElement(name = "Imposta")
    public String getTax() {
        return this.nFormatter.format(this.tax);
    }

    /**
     * @return the vatCollectability
     */
    @XmlElement(name = "EsigibilitaIVA")
    public String getVatCollectability() {
        if (this.vatCollectability != null) {
            return this.vatCollectability.getVal();
        }
        return null;
    }

    /**
     * @return the lawReference
     */
    @XmlElement(name = "RiferimentoNormativo")
    public String getLawReference() {
        return this.lawReference;
    }

    /**
     * @author Aliseo-G
     *
     */
    public static final class SummaryBuilder {

        /**
         * VAT percentage.
         */
        private final transient double vatPercentage;

        /**
         * VAT Nature.
         */
        private transient String vatNature;

        /**
         * Rounding.
         */
        private transient double additionalCosts;

        /**
         * Rounding.
         */
        private transient double rounding;

        /**
         * Taxable amount.
         */
        private final transient double taxable;

        /**
         * Tax amount.
         */
        private final transient double tax;

        /**
         * VAT collectability.
         */
        private transient VATCollectability vatCollectability;

        /**
         * Law reference.
         */
        private transient String lawReference;

        /**
         * Creates a {@link SummaryBuilder} with the mandatory fields.
         * 
         * @param perc
         *            vat percentage
         * @param taxableAmount
         *            taxable amount
         * @param taxAmount
         *            tax
         */
        public SummaryBuilder(final double perc, final double taxableAmount,
                final double taxAmount) {
            super();
            this.vatPercentage = perc;
            this.taxable = taxableAmount;
            this.tax = taxAmount;
        }

        /**
         * @param nature
         *            the vatNature to set
         * @return {@link SummaryBuilder} with VAT nature set
         */
        public SummaryBuilder withVatNature(final String nature) {
            this.vatNature = nature;
            return this;
        }

        /**
         * @param costs
         *            the additional costs to set
         * @return {@link SummaryBuilder} with additional costs set
         */
        public SummaryBuilder withAdditionalCosts(final double costs) {
            this.additionalCosts = costs;
            return this;
        }

        /**
         * @param roundingAmount
         *            the rounding to set
         * @return {@link SummaryBuilder} with rounding amount set
         */
        public SummaryBuilder withRounding(final double roundingAmount) {
            this.rounding = roundingAmount;
            return this;
        }

        /**
         * @param collectability
         *            the vatCollectability to set
         * @return {@link SummaryBuilder} with VAT Collectability set
         */
        public SummaryBuilder withVatCollectability(
                final VATCollectability collectability) {
            this.vatCollectability = collectability;
            return this;
        }

        /**
         * @param ref
         *            the lawReference to set
         * @return {@link SummaryBuilder} with law reference set
         */
        public SummaryBuilder withLawReference(final String ref) {
            this.lawReference = ref;
            return this;
        }

        /**
         * @return the new {@link Summary} instance with the attribute set
         */
        public Summary build() {
            return new Summary(this);
        }

        /**
         * @return the vatPercentage
         */
        private double getVatPercentage() {
            return this.vatPercentage;
        }

        /**
         * @return this.the vatNature
         */
        private String getVatNature() {
            return this.vatNature;
        }

        /**
         * @return the rounding
         */
        private double getRounding() {
            return this.rounding;
        }

        /**
         * @return the taxable
         */
        private double getTaxable() {
            return this.taxable;
        }

        /**
         * @return the tax
         */
        private double getTax() {
            return this.tax;
        }

        /**
         * @return the vatCollectability
         */
        private VATCollectability getVatCollectability() {
            return this.vatCollectability;
        }

        /**
         * @return the lawReference
         */
        private String getLawReference() {
            return this.lawReference;
        }

        /**
         * 
         * @return the additional costs
         */
        private double getAdditionalCosts() {
            return this.additionalCosts;
        }
    }

}
