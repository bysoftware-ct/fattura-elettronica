/**
 *
 */
package it.bysoftware.ct.electronicinvoice.body.generaldata;

import it.bysoftware.ct.electronicinvoice.utils.Constants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class represents a transport document.
 *
 * @author Mario Torrisi
 *
 */
@XmlType(propOrder = {"documentNumber", "documentDate", "lineReferences"})
public final class TransportDocument {

    /**
     * Date formatter.
     */
    private final transient SimpleDateFormat dFormatter;

    /**
     * The actual address.
     */
    private transient String documentNumber;

    /**
     * The actual address.
     */
    private transient Date documentDate;

    /**
     * The actual address.
     */
    private transient List<Integer> lineReferences;

    /**
     * This class cannot be directly instantiated, please refer to
     * {@link TransportDocumentBuilder}.
     */
    public TransportDocument() {
        super();
        this.dFormatter = new SimpleDateFormat(Constants.DATE_PATTERN,
                Locale.US);
    }

    /**
     * This class cannot be directly instantiated, please refer to
     * {@link AddressBuilder}.
     *
     * @param builder the builder
     */
    protected TransportDocument(final TransportDocumentBuilder builder) {
        this();
        this.documentNumber = builder.getDocumentNumber();
        this.documentDate = builder.getDocumentDate();
        this.lineReferences = builder.getLineReferences();
    }

    /**
     * Returns teh transport document number.
     *
     * @return the number
     */
    @XmlElement(name = "NumeroDDT")
    public String getDocumentNumber() {
        return documentNumber;
    }

    /**
     * Return the transport document date.
     *
     * @return the date
     */
    @XmlElement(name = "DataDDT")
    public String getDocumentDate() {
        return this.dFormatter.format(this.documentDate);
    }

    /**
     * Returns the line number references.
     *
     * @return the list of line number references
     */
    @XmlElement(name = "RiferimentoNumeroLinea")
    public List<Integer> getLineReferences() {
        return lineReferences;
    }

    /**
     * This class allows to build an {@link TransportDocument}.
     *
     * @author Mario Torrisi
     *
     */
    public static final class TransportDocumentBuilder {

        /**
         * The actual address.
         */
        private final transient String documentNumber;

        /**
         * The actual address.
         */
        private final transient Date documentDate;

        /**
         * The actual address.
         */
        private transient List<Integer> lineReferences;

        /**
         * Creates an {@link TransportDocumentBuilder} object with the mandatory
         * fields.
         *
         * @param number the documen number
         * @param date the documment date
         */
        public TransportDocumentBuilder(final String number, final Date date) {
            this.documentNumber = number;
            this.documentDate = date;
        }

        /**
         * Builds the actual {@link TransportDocument} object with the specified
         * fields (optional and mandatory).
         *
         * @return build the Address
         */
        public TransportDocument build() {
            return new TransportDocument(this);
        }

        /**
         * Sets the {@link List} of line reference numbers.
         *
         * @param number the line refeences number
         * @return {@link List} of line refernece number
         */
        public TransportDocumentBuilder withLineReference(final int number) {
            if (this.lineReferences == null) {
                this.lineReferences = new ArrayList<>();
            }
            this.lineReferences.add(number);
            return this;
        }

        /**
         * Returns document number.
         *
         * @return the document number
         */
        public String getDocumentNumber() {
            return this.documentNumber;
        }

        /**
         * Returns document date.
         *
         * @return the document date
         */
        public Date getDocumentDate() {
            return this.documentDate;
        }

        /**
         * Returns {@link List} of reference line numbers.
         *
         * @return the {@link List} of reference line numbers
         */
        public List<Integer> getLineReferences() {
            return this.lineReferences;
        }

    }
}
