/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.body;

import it.bysoftware.ct.electronicinvoice.body.goodsandservices.RowDetail;
import it.bysoftware.ct.electronicinvoice.body.goodsandservices.Summary;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = { "rows", "summaries" })
public final class GoodsServices {

    /**
     * The document rows {@link RowDetail}.
     */
    private transient List<RowDetail> rows;

    /**
     * The document summaries {@link Summary}.
     */
    private transient List<Summary> summaries;

    /**
     * This class requires two mandatory attributes.
     */
    private GoodsServices() {
        super();
    }

    /**
     * Creates a new {@link GoodsServices} instance.
     * 
     * @param rowList
     *            the document rows
     * @param summaryList
     *            the document summaries
     */
    public GoodsServices(final List<RowDetail> rowList,
            final List<Summary> summaryList) {
        this();
        this.rows = rowList;
        this.summaries = summaryList;
    }

    /**
     * @return the rows
     */
    @XmlElement(name = "DettaglioLinee")
    public List<RowDetail> getRows() {
        return rows;
    }

    /**
     * @return the summaries
     */
    @XmlElement(name = "DatiRiepilogo")
    public List<Summary> getSummaries() {
        return summaries;
    }

}
