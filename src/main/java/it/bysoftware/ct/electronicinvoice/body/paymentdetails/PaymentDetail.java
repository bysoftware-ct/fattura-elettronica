/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.bysoftware.ct.electronicinvoice.body.paymentdetails;

import it.bysoftware.ct.electronicinvoice.utils.Constants;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Mario
 */
@XmlType(propOrder = {"payee", "paymentType", "paymentStartDate", "paymentdays",
    "paymentExpirDate", "paymentAmount", "postOfficeCode", "surname", "name",
    "fiscalCode", "title", "institute", "iban", "abi", "cab", "bic",
    "advancePayDisc", "advancePayLimitDate", "delayedPaymentCosts",
    "costsStartDate", "paymentCode"})
public final class PaymentDetail {

    /**
     * Number formatter.
     */
    private final transient DecimalFormat nFormatter;

    /**
     *
     */
    private final transient SimpleDateFormat dFormatter;

    /**
     *
     */
    private transient String payee;

    /**
     *
     */
    private transient String paymentType;

    /**
     *
     */
    private transient Date paymentStartDate;

    /**
     *
     */
    private transient int paymentdays;

    /**
     *
     */
    private transient Date paymentExpirDate;

    /**
     *
     */
    private transient double paymentAmount;

    /**
     *
     */
    private transient String postOfficeCode;

    /**
     *
     */
    private transient String surname;

    /**
     *
     */
    private transient String name;

    /**
     *
     */
    private transient String fiscalCode;

    /**
     *
     */
    private transient String title;

    /**
     *
     */
    private transient String institute;

    /**
     *
     */
    private transient String iban;

    /**
     *
     */
    private transient String abi;

    /**
     *
     */
    private transient String cab;

    /**
     *
     */
    private transient String bic;

    /**
     *
     */
    private transient double advancePayDisc;

    /**
     *
     */
    private transient Date advancePayLimitDate;

    /**
     *
     */
    private transient double delayedPaymentCosts;

    /**
     *
     */
    private transient Date costsStartDate;

    /**
     *
     */
    private transient String paymentCode;

    private PaymentDetail() {
        super();
        this.nFormatter = new DecimalFormat(Constants.DOUBLE_PATTERN,
                DecimalFormatSymbols.getInstance(Locale.US));
        this.dFormatter = new SimpleDateFormat(Constants.DATE_PATTERN,
                Locale.US);
    }

    public PaymentDetail(PaymentDetailsBuilder builder) {
        this();
        this.payee = builder.getPayee();
        this.paymentType = builder.getPaymentType();
        this.paymentStartDate = builder.getPaymentStartDate();
        this.paymentdays = builder.getPaymentdays();
        this.paymentExpirDate = builder.getPaymentExpirDate();
        this.paymentAmount = builder.getPaymentAmount();
        this.postOfficeCode = builder.getPostOfficeCode();
        this.surname = builder.getSurname();
        this.name = builder.getName();
        this.fiscalCode = builder.getFiscalCode();
        this.title = builder.getTitle();
        this.institute = builder.getInstitute();
        this.iban = builder.getIban();
        this.abi = builder.getAbi();
        this.cab = builder.getCab();
        this.bic = builder.getBic();
        this.advancePayDisc = builder.getAdvancePayDisc();
        this.advancePayLimitDate = builder.getAdvancePayLimitDate();
        this.delayedPaymentCosts = builder.getDelayedPaymentCosts();
        this.costsStartDate = builder.getCostsStartDate();
        this.paymentCode = builder.getPaymentCode();
    }

    /**
     * @return the payee
     */
    @XmlElement(name = "Beneficiario")
    public String getPayee() {
        return this.payee;
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "ModalitaPagamento")
    public String getPaymentType() {
        return this.paymentType;
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "DataRiferimentoTerminiPagamento")
    public String getPaymentStartDate() {
        if (this.paymentStartDate != null) {
            return this.dFormatter.format(this.paymentStartDate);
        } else {
            return null;
        }
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "GiorniTerminiPagamento")
    public String getPaymentdays() {
        if (this.paymentdays > 0) {
            return String.valueOf(this.paymentdays);
        } else {
            return null;
        }
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "DataScadenzaPagamento")
    public String getPaymentExpirDate() {
        if (this.paymentExpirDate != null) {
            return this.dFormatter.format(this.paymentExpirDate);
        } else {
            return null;
        }
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "ImportoPagamento")
    public String getPaymentAmount() {
        if (this.paymentAmount > 0) {
            return this.nFormatter.format(this.paymentAmount);
        } else {
            return null;
        }
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "CodUfficioPostale")
    public String getPostOfficeCode() {
        return this.postOfficeCode;
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "CognomeQuietanzante")
    public String getSurname() {
        return this.surname;
    }

    @XmlElement(name = "NomeQuietanzante")
    public String getName() {
        return this.name;
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "CFQuietanzante")
    public String getFiscalCode() {
        return this.fiscalCode;
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "TitoloQuietanzante")
    public String getTitle() {
        return this.title;
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "IstitutoFinanziario")
    public String getInstitute() {
        return this.institute;
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "IBAN")
    public String getIban() {
        return this.iban;
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "ABI")
    public String getAbi() {
        return this.abi;
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "CAB")
    public String getCab() {
        return this.cab;
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "BIC")
    public String getBic() {
        return this.bic;
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "ScontoPagamentoAnticipato")
    public String getAdvancePayDisc() {
        if (this.advancePayDisc > 0) {
            return this.nFormatter.format(this.advancePayDisc);
        } else {
            return null;
        }
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "DataLimitePagamentoAnticipato")
    public String getAdvancePayLimitDate() {
        if (this.advancePayLimitDate != null) {
            return this.dFormatter.format(this.advancePayLimitDate);
        } else {
            return null;
        }
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "PenalitaPagamentiRitardati")
    public String getDelayedPaymentCosts() {
        if (this.delayedPaymentCosts > 0) {
            return this.nFormatter.format(this.delayedPaymentCosts);
        } else {
            return null;
        }
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "DataDecorrenzaPenale")
    public String getCostsStartDate() {
        if (this.costsStartDate != null) {
            return this.dFormatter.format(this.costsStartDate);
        } else {
            return null;
        }
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "CodicePagamento")
    public String getPaymentCode() {
        return this.paymentCode;
    }

    public static class PaymentDetailsBuilder {

        /**
         *
         */
        private transient String payee;

        /**
         *
         */
        private final transient String paymentType;

        /**
         *
         */
        private transient Date paymentStartDate;

        /**
         *
         */
        private transient int paymentdays;

        /**
         *
         */
        private transient Date paymentExpirDate;

        /**
         *
         */
        private final transient double paymentAmount;

        /**
         *
         */
        private transient String postOfficeCode;

        /**
         *
         */
        private transient String surname;

        /**
         *
         */
        private transient String name;

        /**
         *
         */
        private transient String fiscalCode;

        /**
         *
         */
        private transient String title;

        /**
         *
         */
        private transient String institute;

        /**
         *
         */
        private transient String iban;

        /**
         *
         */
        private transient String abi;

        /**
         *
         */
        private transient String cab;

        /**
         *
         */
        private transient String bic;

        /**
         *
         */
        private transient double advancePayDisc;

        /**
         *
         */
        private transient Date advancePayLimitDate;

        /**
         *
         */
        private transient double delayedPaymentCosts;

        /**
         *
         */
        private transient Date costsStartDate;

        /**
         *
         */
        private transient String paymentCode;

        public PaymentDetailsBuilder(
                final String type, final double amount) {
            this.paymentType = type;
            this.paymentAmount = amount;
        }

        public PaymentDetailsBuilder withPayee(
                final String payee) {
            this.payee = payee;
            return this;
        }

        public PaymentDetailsBuilder withPaymentStartDate(
                final Date paymentStartDate) {
            this.paymentStartDate = paymentStartDate;
            return this;
        }

        public PaymentDetailsBuilder withPaymentdays(
                final int paymentdays) {
            this.paymentdays = paymentdays;
            return this;
        }

        public PaymentDetailsBuilder withPaymentExpirDate(
                final Date paymentExpirDate) {
            this.paymentExpirDate = paymentExpirDate;
            return this;
        }

        public PaymentDetailsBuilder withPostOfficeCode(
                final String postOfficeCode) {
            this.postOfficeCode = postOfficeCode;
            return this;
        }

        public PaymentDetailsBuilder withSurname(
                final String surname) {
            this.surname = surname;
            return this;
        }

        public PaymentDetailsBuilder withName(
                final String name) {
            this.name = name;
            return this;
        }

        public PaymentDetailsBuilder withFiscalCode(
                final String fiscalCode) {
            this.fiscalCode = fiscalCode;
            return this;
        }

        public PaymentDetailsBuilder withTitle(
                final String title) {
            this.title = title;
            return this;
        }

        public PaymentDetailsBuilder withInstitute(
                final String institute) {
            this.institute = institute;
            return this;
        }

        public PaymentDetailsBuilder withIban(
                final String iban) {
            this.iban = iban;
            return this;
        }

        public PaymentDetailsBuilder withAbi(
                final String abi) {
            this.abi = abi;
            return this;
        }

        public PaymentDetailsBuilder withCab(
                final String cab) {
            this.cab = cab;
            return this;
        }

        public PaymentDetailsBuilder withBic(
                final String bic) {
            this.bic = bic;
            return this;
        }

        public PaymentDetailsBuilder withAdvancePayDisc(
                final double advancePayDisc) {
            this.advancePayDisc = advancePayDisc;
            return this;
        }

        public PaymentDetailsBuilder withAdvancePayLimitDate(
                final Date advancePayLimitDate) {
            this.advancePayLimitDate = advancePayLimitDate;
            return this;
        }

        public PaymentDetailsBuilder withDelayedPaymentCosts(
                final double delayedPaymentCosts) {
            this.delayedPaymentCosts = delayedPaymentCosts;
            return this;
        }

        public PaymentDetailsBuilder withCostsStartDate(
                final Date costsStartDate) {
            this.costsStartDate = costsStartDate;
            return this;
        }

        public PaymentDetailsBuilder withPaymentCode(final String paymentCode) {
            this.paymentCode = paymentCode;
            return this;
        }

        public PaymentDetail build() {
            return new PaymentDetail(this);
        }

        String getPayee() {
            return this.payee;
        }

        String getPaymentType() {
            return this.paymentType;
        }

        Date getPaymentStartDate() {
            return this.paymentStartDate;
        }

        int getPaymentdays() {
            return this.paymentdays;
        }

        Date getPaymentExpirDate() {
            return this.paymentExpirDate;
        }

        double getPaymentAmount() {
            return this.paymentAmount;
        }

        String getPostOfficeCode() {
            return this.postOfficeCode;
        }

        String getSurname() {
            return this.surname;
        }

        String getName() {
            return this.name;
        }

        String getFiscalCode() {
            return this.fiscalCode;
        }

        String getTitle() {
            return this.title;
        }

        String getInstitute() {
            return this.institute;
        }

        String getIban() {
            return this.iban;
        }

        String getAbi() {
            return this.abi;
        }

        String getCab() {
            return this.cab;
        }

        String getBic() {
            return this.bic;
        }

        double getAdvancePayDisc() {
            return this.advancePayDisc;
        }

        Date getAdvancePayLimitDate() {
            return this.advancePayLimitDate;
        }

        double getDelayedPaymentCosts() {
            return this.delayedPaymentCosts;
        }

        Date getCostsStartDate() {
            return this.costsStartDate;
        }

        String getPaymentCode() {
            return this.paymentCode;
        }

    }

}
