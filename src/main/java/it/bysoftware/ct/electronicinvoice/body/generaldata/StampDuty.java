/**
 *
 */
package it.bysoftware.ct.electronicinvoice.body.generaldata;

import it.bysoftware.ct.electronicinvoice.utils.Constants;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = {"yes", "amount"})
public class StampDuty {

    /**
     * Number formatter for unit price.
     */
    private final String yes = "SI";

    /**
     * Number formatter for unit price.
     */
    private final transient DecimalFormat nFormatter;

    /**
     *
     */
    private double amount;

    private StampDuty() {
        super();
        this.nFormatter = new DecimalFormat(Constants.DOUBLE_PATTERN,
                DecimalFormatSymbols.getInstance(Locale.US));
    }
    
    public StampDuty(Double value) {
        this();
        this.amount = value;
    }

    /**
     * @return the unitPrice
     */
    @XmlElement(name = "BolloVirtuale")
    public String getYes() {
        return this.yes;
    }

    /**
     * @return the unitPrice
     */
    @XmlElement(name = "ImportoBollo")
    public String getAmount() {
        return this.nFormatter.format(this.amount);
    }

}
