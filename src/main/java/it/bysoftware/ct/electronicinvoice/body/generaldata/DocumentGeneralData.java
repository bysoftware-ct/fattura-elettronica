/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.body.generaldata;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import it.bysoftware.ct.electronicinvoice.utils.Constants;
import it.bysoftware.ct.electronicinvoice.utils.Utils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = { "docType", "currencyCode", "docDate", "invoiceNumber",
        "withholding", "stampDuty", "pensionFund", "discOrSur", "totalAmount",
        "rounding", "causal", "art73" })
public final class DocumentGeneralData {

    /**
     * Number formatter.
     */
    private final transient DecimalFormat nFormatter;
    
    /**
     * Number formatter.
     */
    private final transient DecimalFormat tFormatter;
    
    /**
     * Number formatter.
     */
    private final transient SimpleDateFormat dFormatter;
    
    /**
     * The max number of character for casual attribute.
     */
    private static final int CAUSAL_CHAR_LIMIT = 200;

    /**
     * The document type .
     */
    private transient String docType;

    /**
     * The Currency code standard ISO 4217 alpha-3:2001.
     */
    private transient String currencyCode;

    /**
     * The document date.
     */
    private transient Date docDate;

    /**
     * Invoice number.
     */
    private transient String invoiceNumber;

    /**
     * The withholding data.
     */
    private transient Withholding withholding;

    /**
     * The stamp duty data.
     */
    private transient StampDuty stampDuty;

    /**
     * The pension fund data.
     */
    private transient PensionFund pensionFund;

    /**
     * The discount or surcharge data.
     */
    private transient DiscountSurcharge discOrSur;

    /**
     * The document total amount.
     */
    private transient double totalAmount;

    /**
     * The document rounding amount.
     */
    private transient double rounding;

    /**
     * The document causal.
     */
    private transient String causal;

    /**
     * The document is created following art.37 law.
     */
    private transient boolean art73;

    /**
     * This class cannot be directly instantiated, please refer to
     * {@link DocumentGeneralDataBuilder}.
     */
    private DocumentGeneralData() {
        super();
        this.tFormatter = new DecimalFormat(Constants.DOUBLE_PATTERN,
                DecimalFormatSymbols.getInstance(Locale.US));
        this.nFormatter = new DecimalFormat(Constants.QUANTITY_PATTERN,
                DecimalFormatSymbols.getInstance(Locale.US));
        this.dFormatter = new SimpleDateFormat(Constants.DATE_PATTERN,
                Locale.US);
    }

    /**
     * This class cannot be directly instantiated, please refer to
     * {@link DocumentGeneralDataBuilder}.
     * 
     * @param builder
     *            the builder
     */
    protected DocumentGeneralData(final DocumentGeneralDataBuilder builder) {
        this();
        this.docType = builder.getDocType();
        this.currencyCode = builder.getCurrencyCode();
        this.docDate = builder.getDocDate();
        this.invoiceNumber = builder.getInvoiceNumber();
        this.withholding = builder.getWithholding();
        this.stampDuty = builder.getStampDuty();
        this.pensionFund = builder.getPensionFund();
        this.discOrSur = builder.getDiscOrSur();
        this.totalAmount = builder.getTotalAmount();
        this.rounding = builder.getRounding();
        this.causal = builder.getCausal();
        this.art73 = builder.isArt73();
    }

    /**
     * @return the docType
     */
    @XmlElement(name = "TipoDocumento")
    public String getDocType() {
        return this.docType;
    }

    /**
     * @return the currencyCode
     */
    @XmlElement(name = "Divisa")
    public String getCurrencyCode() {
        return this.currencyCode;
    }

    /**
     * @return the docDate
     */
    @XmlElement(name = "Data")
    public String getDocDate() {
        return this.dFormatter.format(this.docDate);
    }

    /**
     * @return the invoiceNumber
     */
    @XmlElement(name = "Numero")
    public String getInvoiceNumber() {
        return this.invoiceNumber;
    }

    /**
     * @return the withholding
     */
    @XmlElement(name = "DatiRitenuta")
    public Withholding getWithholding() {
        return this.withholding;
    }

    /**
     * @return the stampDuty
     */
    @XmlElement(name = "DatiBollo")
    public StampDuty getStampDuty() {
        return this.stampDuty;
    }

    /**
     * @return the pensionFund
     */
    @XmlElement(name = "DatiCassaPrevidenziale")
    public PensionFund getPensionFund() {
        return this.pensionFund;
    }

    /**
     * @return the discount or surcharge
     */
    @XmlElement(name = "ScontoMaggiorazione")
    public DiscountSurcharge getDiscOrSur() {
        return this.discOrSur;
    }

    /**
     * @return the totalAmount
     */
    @XmlElement(name = "ImportoTotaleDocumento")
    public String getTotalAmount() {
        String res;
        if (this.totalAmount == 0) {
            res = null;
        } else {
            res = this.tFormatter.format(this.totalAmount);
        }
        return res;
    }

    /**
     * @return the rounding
     */
    @XmlElement(name = "Arrotondamento")
    public String getRounding() {
        String res;
        if (this.rounding == 0) {
            res = null;
        } else {
            res = this.nFormatter.format(this.rounding);
        }
        return res;
    }

    /**
     * @return the causal
     */
    @XmlElement(name = "Causale")
    public List<String> getCausal() {
        return Utils.splitToNChar(this.causal,
                DocumentGeneralData.CAUSAL_CHAR_LIMIT);
    }

    /**
     * @return the art73
     */
    @XmlElement(name = "Art73")
    public String isArt73() {
        String res;
        if (this.art73) {
            res = "SI";
        } else {
            res = null;
        }
        return res;
    }

    /**
     * @author Aliseo-G
     *
     */
    public static final class DocumentGeneralDataBuilder {

        /**
         * The document type.
         */
        private final transient String docType;

        /**
         * The Currency code standard ISO 4217 alpha-3:2001.
         */
        private final transient String currency;

        /**
         * The document date.
         */
        private final transient Date docDate;

        /**
         * Invoice number.
         */
        private final transient String invoiceNumber;

        /**
         * The withholding data.
         */
        private transient Withholding withholding;

        /**
         * The stamp duty data.
         */
        private transient StampDuty stampDuty;

        /**
         * The pension fund data.
         */
        private transient PensionFund pensionFund;

        /**
         * The surcharge data.
         */
        private transient DiscountSurcharge discOrSur;

        /**
         * The document total amount.
         */
        private transient double totalAmount;

        /**
         * The document rounding amount.
         */
        private transient double rounding;

        /**
         * The document causal.
         */
        private transient String causal;

        /**
         * The document is created following art.37 law.
         */
        private transient boolean art73;

        /**
         * Creates an {@link DocumentGeneralDataBuilder} object with the
         * mandatory fields.
         * 
         * @param type
         *            the document type
         * @param currencyCode
         *            a string the represent the currency code in ISO 4217
         *            alpha-3:2001 standard
         * @param date
         *            the document date
         * @param number
         *            the invoice number
         */
        public DocumentGeneralDataBuilder(final String type,
                final String currencyCode, final Date date,
                final String number) {
            super();
            this.docType = type;
            this.currency = currencyCode;
            this.docDate = date;
            this.invoiceNumber = number;
        }

        /**
         * Sets Withholding value.
         * 
         * @param value
         *            the Withholding value to set
         * @return a {@link DocumentGeneralDataBuilder} instance with
         *         Withholding set.
         */
        public DocumentGeneralDataBuilder withWithholding(
                final Withholding value) {
            this.withholding = value;
            return this;
        }

        /**
         * Sets stamp duty value.
         * 
         * @param value
         *            the stamp duty value to set
         * @return a {@link DocumentGeneralDataBuilder} instance with stamp duty
         *         set.
         */
        public DocumentGeneralDataBuilder withStampDuty(final StampDuty value) {
            this.stampDuty = value;
            return this;
        }

        /**
         * Sets pension fund value.
         * 
         * @param value
         *            the pension fund value to set
         * @return a {@link DocumentGeneralDataBuilder} instance with pension
         *         fund set.
         */
        public DocumentGeneralDataBuilder withPensionFund(
                final PensionFund value) {
            this.pensionFund = value;
            return this;
        }

        /**
         * Sets discount or surcharge value.
         * 
         * @param value
         *            the discount or surcharge value to set
         * @return a {@link DocumentGeneralDataBuilder} instance with discount
         *         or surcharge value set.
         */
        public DocumentGeneralDataBuilder withDiscountOrSurcharge(
                final DiscountSurcharge value) {
            this.discOrSur = value;
            return this;
        }

        /**
         * Sets document total amount value.
         * 
         * @param value
         *            the document total amount value to set
         * @return a {@link DocumentGeneralDataBuilder} instance with document
         *         total amount value to set.
         */
        public DocumentGeneralDataBuilder withTotalAmount(final double value) {
            this.totalAmount = value;
            return this;
        }

        /**
         * Sets rounding value .
         * 
         * @param value
         *            the document causal description to set
         * @return a {@link DocumentGeneralDataBuilder} instance with document
         *         causal description to set.
         */
        public DocumentGeneralDataBuilder withRounding(final double value) {
            this.rounding = value;
            return this;
        }

        /**
         * Sets document causal.
         * 
         * @param description
         *            the document causal description to set
         * @return a {@link DocumentGeneralDataBuilder} instance with document
         *         causal description to set.
         */
        public DocumentGeneralDataBuilder withCausal(final String description) {
            this.causal = description;
            return this;
        }

        /**
         * true if document is created using the art. 73 law, false otherwise.
         * 
         * @param value
         *            the art73 to set
         * @return a {@link DocumentGeneralDataBuilder} instance with art 73
         *         set.
         */
        public DocumentGeneralDataBuilder withArt73(final boolean value) {
            this.art73 = value;
            return this;
        }

        /**
         * Builds the actual {@link DocumentGeneralData} object with the
         * specified fields (optional and mandatory).
         * 
         * @return build the {@link DocumentGeneralData}
         */
        public DocumentGeneralData build() {
            return new DocumentGeneralData(this);
        }

        /**
         * @return the docType
         */
        private String getDocType() {
            return this.docType;
        }

        /**
         * @return the currency code
         */
        private String getCurrencyCode() {
            return this.currency;
        }

        /**
         * @return the docDate
         */
        private Date getDocDate() {
            return this.docDate;
        }

        /**
         * @return the invoiceNumber
         */
        private String getInvoiceNumber() {
            return this.invoiceNumber;
        }

        /**
         * @return the withholding
         */
        private Withholding getWithholding() {
            return this.withholding;
        }

        /**
         * @return the stampDuty
         */
        private StampDuty getStampDuty() {
            return this.stampDuty;
        }

        /**
         * @return the pensionFund
         */
        private PensionFund getPensionFund() {
            return this.pensionFund;
        }

        /**
         * @return the surcharge
         */
        private DiscountSurcharge getDiscOrSur() {
            return this.discOrSur;
        }

        /**
         * @return the totalAmount
         */
        private double getTotalAmount() {
            return this.totalAmount;
        }

        /**
         * @return the rounding
         */
        private double getRounding() {
            return this.rounding;
        }

        /**
         * @return the causal
         */
        private String getCausal() {
            return this.causal;
        }

        /**
         * @return the art73
         */
        private boolean isArt73() {
            return this.art73;
        }

    }
}
