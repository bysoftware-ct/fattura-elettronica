/**
 *
 */
package it.bysoftware.ct.electronicinvoice.body.goodsandservices;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import it.bysoftware.ct.electronicinvoice.body.generaldata.DiscountSurcharge;
import it.bysoftware.ct.electronicinvoice.utils.Constants;
import it.bysoftware.ct.electronicinvoice.utils.LineType;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = {"lineNumber", "lineType", "itemCode", "description",
    "quantity", "measureUnit", "startPeriod", "endPeriod", "unitPrice",
    "discOrSur", "totalPrice", "vatPercentage", "withholding", "vatNature",
    "adminRef", "otherData"})
public final class RowDetail {

    /**
     * Number formatter.
     */
    private final transient DecimalFormat nFormatter;

    /**
     * Number formatterfor quantity.
     */
    private final transient DecimalFormat qFormatter;

    /**
     * Number formatter for unit price.
     */
    private final transient DecimalFormat pFormatter;
    /**
     *
     */
    private final transient SimpleDateFormat dFormatter;

    /**
     * Line number.
     */
    private transient int lineNumber;

    /**
     * Line type.
     */
    private transient LineType lineType;

    /**
     * Item code.
     */
    private transient ItemCode itemCode;

    /**
     * Description.
     */
    private transient String description;

    /**
     * Quantity.
     */
    private transient double quantity;

    /**
     * Measure unit.
     */
    private transient String measureUnit;

    /**
     * Measure unit.
     */
    private transient Date startPeriod;

    /**
     * Measure unit.
     */
    private transient Date endPeriod;

    /**
     * Unit price.
     */
    private transient double unitPrice;

    /**
     * Discount or surcharge.
     */
    private transient List<DiscountSurcharge> discOrSur;

    /**
     * Total price.
     */
    private transient double totalPrice;

    /**
     * VAT percentage.
     */
    private transient double vatPercentage;

    /**
     * True if the document is a withholding.
     */
    private transient boolean withholding;

    /**
     * VAT Nature.
     */
    private transient String vatNature;

    /**
     * Administrative reference.
     */
    private transient String adminRef;

    /**
     * Other managment data.
     */
    private transient OtherManagmentData otherData;

    /**
     *
     */
    private RowDetail() {
        super();
        this.nFormatter = new DecimalFormat(Constants.DOUBLE_PATTERN,
                DecimalFormatSymbols.getInstance(Locale.US));
        this.qFormatter = new DecimalFormat(Constants.QUANTITY_PATTERN,
                DecimalFormatSymbols.getInstance(Locale.US));
        this.pFormatter = new DecimalFormat(Constants.PRICE_PATTERN,
                DecimalFormatSymbols.getInstance(Locale.US));
        this.dFormatter = new SimpleDateFormat(Constants.DATE_PATTERN,
                Locale.US);
    }

    /**
     * @param builder the builder
     */
    protected RowDetail(final RowDetailBuilder builder) {
        this();
        this.lineNumber = builder.getLineNumber();
        this.lineType = builder.getLineType();
        this.itemCode = builder.getItemCode();
        this.description = builder.getDescription();
        this.quantity = builder.getQuantity();
        this.measureUnit = builder.getMeasureUnit();
        this.startPeriod = builder.getStartPeriod();
        this.endPeriod = builder.getEndPeriod();
        this.unitPrice = builder.getUnitPrice();
        this.discOrSur = builder.getDiscOrSur();
        this.totalPrice = builder.getTotalPrice();
        this.vatPercentage = builder.getVatPercentage();
        this.withholding = builder.isWithholding();
        this.vatNature = builder.getVatNature();
        this.adminRef = builder.getAdminRef();
        this.otherData = builder.getOtherData();
    }

    /**
     * <p>
     * Converts a boolean to a String returning one of the input Strings.
     * </p>
     *
     * <pre>
     *   BooleanUtils.toString(true, "true", "false")   = "true"
     *   BooleanUtils.toString(false, "true", "false")  = "false"
     * </pre>
     *
     * @param bool the Boolean to check
     * @param trueString the String to return if <code>true</code>, may be
     * <code>null</code>
     * @param falseString the String to return if <code>false</code>, may be
     * <code>null</code>
     * @return one of the two input Strings
     */
    private String toString(final boolean bool, final String trueString,
            final String falseString) {
        String res;
        if (bool) {
            res = trueString;
        } else {
            res = falseString;
        }
        return res;
    }

    /**
     * @return the lineNumber
     */
    @XmlElement(name = "NumeroLinea")
    public int getLineNumber() {
        return this.lineNumber;
    }

    /**
     * @return the lineType
     */
    @XmlElement(name = "TipoCessionePrestazione")
    public String getLineType() {
        String res;
        if (this.lineType == null) {
            res = null;
        } else {
            res = this.lineType.getVal();
        }
        return res;
    }

    /**
     * @return the item code
     */
    @XmlElement(name = "CodiceArticolo")
    public ItemCode getItemCode() {
        return this.itemCode;
    }

    /**
     * @return the description
     */
    @XmlElement(name = "Descrizione")
    public String getDescription() {
        return this.description;
    }

    /**
     * @return the quantity
     */
    @XmlElement(name = "Quantita")
    public String getQuantity() {
        String res;
        if (this.quantity == 0) {
            res = null;
        } else {
            res = this.qFormatter.format(this.quantity);
        }
        return res;
    }

    /**
     * @return the measureUnit
     */
    @XmlElement(name = "UnitaMisura")
    public String getMeasureUnit() {
        return this.measureUnit;
    }

    /**
     * @return the startPeriod
     */
    @XmlElement(name = "DataInizioPeriodo")
    public String getStartPeriod() {
        String res;
        if (this.startPeriod == null) {
            res = null;
        } else {
            res = this.dFormatter.format(this.startPeriod);
        }
        return res;
    }

    /**
     * @return the endPeriod
     */
    @XmlElement(name = "DataFinePeriodo")
    public String getEndPeriod() {
        String res;
        if (this.endPeriod == null) {
            res = null;
        } else {
            res = this.dFormatter.format(this.endPeriod);
        }
        return res;
    }

    /**
     * @return the unitPrice
     */
    @XmlElement(name = "PrezzoUnitario")
    public String getUnitPrice() {
        return this.pFormatter.format(this.unitPrice);
    }

    /**
     * @return the discOrSur
     */
    @XmlElement(name = "ScontoMaggiorazione")
    public List<DiscountSurcharge> getDiscOrSur() {
        return this.discOrSur;
    }

    /**
     * @return the totalPrice
     */
    @XmlElement(name = "PrezzoTotale")
    public String getTotalPrice() {
        return this.nFormatter.format(this.totalPrice);
    }

    /**
     * @return the vatPercentage
     */
    @XmlElement(name = "AliquotaIVA")
    public String getVatPercentage() {
        return this.nFormatter.format(this.vatPercentage);
    }

    /**
     * @return the withholding
     */
    @XmlElement(name = "Ritenuta")
    public String isWithholding() {
        return this.toString(this.withholding, "SI", null);
    }

    /**
     * @return the vatNature
     */
    @XmlElement(name = "Natura")
    public String getVatNature() {
        return this.vatNature;
    }

    /**
     * @return the adminRef
     */
    @XmlElement(name = "RiferimentoAmministrazione")
    public String getAdminRef() {
        return this.adminRef;
    }

    /**
     * @return the otherData
     */
    @XmlElement(name = "AltriDatiGestionali")
    public OtherManagmentData getOtherData() {
        return this.otherData;
    }

    /**
     * @author Aliseo-G
     *
     */
    public static final class RowDetailBuilder {

        /**
         * Line number.
         */
        private final transient int lineNumber;

        /**
         * Line type.
         */
        private transient LineType lineType;

        /**
         * Line type.
         */
        private transient ItemCode itemCode;

        /**
         * Description.
         */
        private final transient String description;

        /**
         * Quantity.
         */
        private transient double quantity;

        /**
         * Measure unit.
         */
        private transient String measureUnit;

        /**
         * Measure unit.
         */
        private transient Date startPeriod;

        /**
         * Measure unit.
         */
        private transient Date endPeriod;

        /**
         * Unit price.
         */
        private final transient double unitPrice;

        /**
         * Discount or surcharge.
         */
        private transient List<DiscountSurcharge> discOrSur;

        /**
         * Total price.
         */
        private final transient double totalPrice;

        /**
         * VAT percentage.
         */
        private final transient double vatPercentage;

        /**
         * True if the document is a withholding.
         */
        private transient boolean withholding;

        /**
         * VAT Nature.
         */
        private transient String vatNature;

        /**
         * Administrative reference.
         */
        private transient String adminRef;

        /**
         * Other management data.
         */
        private transient OtherManagmentData otherData;

        /**
         * Creates a {@link RowDetailBuilder}.
         *
         * @param number line number
         * @param descr item description
         * @param unPrice unit price
         * @param total total price
         * @param vatPerc vat percentage
         */
        public RowDetailBuilder(final int number, final String descr,
                final double unPrice, final double total,
                final double vatPerc) {
            super();
            this.lineNumber = number;
            this.description = descr;
            this.unitPrice = unPrice;
            this.totalPrice = total;
            this.vatPercentage = vatPerc;
        }

        /**
         * @param type the lineType to set
         * @return {@link RowDetailBuilder} with type set.
         */
        public RowDetailBuilder withLineType(final LineType type) {
            this.lineType = type;
            return this;

        }

        /**
         * @param code the itemCode to set
         * @return {@link RowDetailBuilder} with item code set.
         */
        public RowDetailBuilder withItemCode(final ItemCode code) {
            this.itemCode = code;
            return this;
        }

        /**
         * @param quant the quantity to set
         * @return {@link RowDetailBuilder} with quantity set.
         */
        public RowDetailBuilder withQuantity(final double quant) {
            this.quantity = quant;
            return this;
        }

        /**
         * @param measUn the measureUnit to set
         * @return {@link RowDetailBuilder} with measure unit set.
         */
        public RowDetailBuilder withMeasureUnit(final String measUn) {
            this.measureUnit = measUn;
            return this;
        }

        /**
         * @param start the startPeriod to set
         * @return {@link RowDetailBuilder} with start period set.
         */
        public RowDetailBuilder withStartPeriod(final Date start) {
            this.startPeriod = start;
            return this;
        }

        /**
         * @param end the endPeriod to set
         * @return {@link RowDetailBuilder} with end period set.
         */
        public RowDetailBuilder withEndPeriod(final Date end) {
            this.endPeriod = end;
            return this;
        }

        /**
         * @param perc the discOrSur to set
         * @return {@link RowDetailBuilder} with discount or surcharge set.
         */
        public RowDetailBuilder withDiscOrSur(final DiscountSurcharge perc) {
            if (this.discOrSur == null) {
                this.discOrSur = new ArrayList<>();
            }
            this.discOrSur.add(perc);
            return this;
        }

        /**
         * true if is a withholding, false otherwise.
         *
         * @param isWithholding the withholding to set
         * @return {@link RowDetailBuilder} with withholding set.
         */
        public RowDetailBuilder withWithholding(final boolean isWithholding) {
            this.withholding = isWithholding;
            return this;
        }

        /**
         * @param nature the vatNature to set
         * @return {@link RowDetailBuilder} with VAT nature set.
         */
        public RowDetailBuilder withVatNature(final String nature) {
            this.vatNature = nature;
            return this;
        }

        /**
         * @param reference the adminRef to set
         * @return {@link RowDetailBuilder} with administrative reference set.
         */
        public RowDetailBuilder withAdminRef(final String reference) {
            this.adminRef = reference;
            return this;
        }

        /**
         * @param data the otherData to set
         * @return {@link RowDetailBuilder} with other management data set.
         */
        public RowDetailBuilder withOtherData(final OtherManagmentData data) {
            this.otherData = data;
            return this;
        }

        /**
         * @return the new {@link RowDetail} instance.
         */
        public RowDetail build() {
            return new RowDetail(this);
        }

        /**
         * @return the lineNumber
         */
        private int getLineNumber() {
            return this.lineNumber;
        }

        /**
         * @return the lineType
         */
        private LineType getLineType() {
            return this.lineType;
        }

        /**
         * @return the itemCode
         */
        private ItemCode getItemCode() {
            return this.itemCode;
        }

        /**
         * @return the description
         */
        private String getDescription() {
            return this.description;
        }

        /**
         * @return the quantity
         */
        private double getQuantity() {
            return this.quantity;
        }

        /**
         * @return the measureUnit
         */
        private String getMeasureUnit() {
            return this.measureUnit;
        }

        /**
         * @return the startPeriod
         */
        private Date getStartPeriod() {
            return this.startPeriod;
        }

        /**
         * @return the endPeriod
         */
        private Date getEndPeriod() {
            return this.endPeriod;
        }

        /**
         * @return the unitPrice
         */
        private double getUnitPrice() {
            return this.unitPrice;
        }

        /**
         * @return the discOrSur
         */
        private List<DiscountSurcharge> getDiscOrSur() {
            return this.discOrSur;
        }

        /**
         * @return the totalPrice
         */
        private double getTotalPrice() {
            return this.totalPrice;
        }

        /**
         * @return the vatPercentage
         */
        private double getVatPercentage() {
            return this.vatPercentage;
        }

        /**
         * @return the withholding
         */
        private boolean isWithholding() {
            return this.withholding;
        }

        /**
         * @return the vatNature
         */
        private String getVatNature() {
            return this.vatNature;
        }

        /**
         * @return the adminRef
         */
        private String getAdminRef() {
            return this.adminRef;
        }

        /**
         * @return the otherData
         */
        private OtherManagmentData getOtherData() {
            return this.otherData;
        }

    }
}
