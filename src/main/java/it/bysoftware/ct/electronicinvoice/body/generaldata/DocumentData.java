/**
 *
 */
package it.bysoftware.ct.electronicinvoice.body.generaldata;

import it.bysoftware.ct.electronicinvoice.utils.Constants;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = {"lineNumberRef", "documentId", "date", "numItem", "code",
    "cup", "cig"})
public class DocumentData {

    /**
     *
     */
    private final transient SimpleDateFormat dFormatter;

    private transient List<Integer> lineNumberRef;

    private transient String documentId;

    private transient Date date;

    private transient String numItem;

    private String code;

    private String cup;

    private String cig;

    /**
     * Get the value of cig
     *
     * @return the value of cig
     */
    @XmlElement(name = "CodiceCIG")
    public String getCig() {
        return cig;
    }

    /**
     * Get the value of cup
     *
     * @return the value of cup
     */
    @XmlElement(name = "CodiceCUP")
    public String getCup() {
        return cup;
    }

    /**
     * Get the value of code
     *
     * @return the value of code
     */
    @XmlElement(name = "CodiceCommessaConvenzione")
    public String getCode() {
        return code;
    }

    /**
     * Get the value of numItem
     *
     * @return the value of numItem
     */
    @XmlElement(name = "NumItem")
    public String getNumItem() {
        return numItem;
    }

    /**
     * Get the value of date
     *
     * @return the value of date
     */
    @XmlElement(name = "Data")
    public String getDate() {
        if (this.date != null) {
            return this.dFormatter.format(this.date);
        } else {
            return null;
        }
    }

    /**
     * Get the value of documentId
     *
     * @return the value of documentId
     */
    @XmlElement(name = "IdDocumento")
    public String getDocumentId() {
        return documentId;
    }

    /**
     * Get the value of lineNumberRef
     *
     * @return the value of lineNumberRef
     */
    @XmlElement(name = "RiferimentoNumeroLinea")
    public List<Integer> getLineNumberRef() {
        return lineNumberRef;
    }

    private DocumentData() {
        super();
        this.dFormatter = new SimpleDateFormat(Constants.DATE_PATTERN,
                Locale.US);
    }

    protected DocumentData(DocumentDataBuilder builder) {
        this();
        this.lineNumberRef = builder.getLineNumberRef();
        this.documentId = builder.getDocumentId();
        this.date = builder.getDate();
        this.numItem = builder.getNumItem();
        this.code = builder.getCode();
        this.cup = builder.getCup();
        this.cig = builder.getCig();
    }

    public static class DocumentDataBuilder {

        private transient List<Integer> lineNumberRef;

        private final transient String documentId;

        private transient Date date;

        private transient String numItem;

        private String code;

        private String cup;

        private String cig;

        public DocumentDataBuilder(final String id) {
            this.documentId = id;
        }

        public DocumentDataBuilder withLineNumberRef(List<Integer> lineNumberRef) {
            this.lineNumberRef = lineNumberRef;
            return this;
        }

        public DocumentDataBuilder withDate(Date date) {
            this.date = date;
            return this;
        }

        public DocumentDataBuilder withNumItem(String numItem) {
            this.numItem = numItem;
            return this;
        }

        public DocumentDataBuilder withCode(String code) {
            this.code = code;
            return this;
        }

        public DocumentDataBuilder withCup(String cup) {
            this.cup = cup;
            return this;
        }

        public DocumentDataBuilder withCig(String cig) {
            this.cig = cig;
            return this;
        }
        
        public DocumentData build() {
            return new DocumentData(this);
        }

        private List<Integer> getLineNumberRef() {
            return this.lineNumberRef;
        }

        private String getDocumentId() {
            return this.documentId;
        }

        private Date getDate() {
            return this.date;
        }

        private String getNumItem() {
            return this.numItem;
        }

        private String getCode() {
            return this.code;
        }

        private String getCup() {
            return this.cup;
        }

        private String getCig() {
            return this.cig;
        }
    }

}
