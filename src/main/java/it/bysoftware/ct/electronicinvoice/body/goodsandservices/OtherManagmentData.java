/**
 *
 */
package it.bysoftware.ct.electronicinvoice.body.goodsandservices;

import it.bysoftware.ct.electronicinvoice.utils.Constants;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = {"type", "textReference", "numberReference",
    "dateReference"})
public class OtherManagmentData {

    /**
     * Type.
     */
    private transient String type;

    /**
     * textReference.
     */
    private transient String textReference;

    /**
     * textReference.
     */
    private transient double numberReference;

    /**
     * textReference.
     */
    private transient Date dateReference;

    /**
     * Number formatter.
     */
    private final transient DecimalFormat nFormatter;

    /**
     *
     */
    private final transient SimpleDateFormat dFormatter;

    public OtherManagmentData() {
        super();
        this.nFormatter = new DecimalFormat(Constants.DOUBLE_PATTERN,
                DecimalFormatSymbols.getInstance(Locale.US));
        this.dFormatter = new SimpleDateFormat(Constants.DATE_PATTERN,
                Locale.US);
    }

    /**
     * @param builder the {@link SummaryBuilder}
     */
    protected OtherManagmentData(final OtherManagmentDataBuilder builder) {
        this();
        this.type = builder.getType();
        this.textReference = builder.getTextReference();
        this.numberReference = builder.getNumberReference();
        this.dateReference = builder.getDateReference();
    }

    @XmlElement(name = "TipoDato")
    public String getType() {
        return this.type;
    }

    @XmlElement(name = "RiferimentoTesto")
    public String getTextReference() {
        return this.textReference;
    }

    @XmlElement(name = "RiferimentoNumero")
    public String getNumberReference() {
        String result;
        if (this.numberReference == 0) {
            result = null;
        } else {
            result = this.nFormatter.format(this.numberReference);
        }
        return result;
    }

    @XmlElement(name = "RiferimentoData")
    public String getDateReference() {
        String res;
        if (this.dateReference == null) {
            res = null;
        } else {
            res = this.dFormatter.format(this.dateReference);
        }
        return res;
    }

    public static class OtherManagmentDataBuilder {

        /**
         * Type.
         */
        private transient String type;

        /**
         * textReference.
         */
        private transient String textReference;

        /**
         * textReference.
         */
        private transient double numberReference;

        /**
         * textReference.
         */
        private transient Date dateReference;

        public OtherManagmentDataBuilder(final String t) {
            this.type = t;
        }

        public String getType() {
            return this.type;
        }

        public void withType(String type) {
            this.type = type;
        }

        public String getTextReference() {
            return textReference;
        }

        public OtherManagmentDataBuilder withTextReference(String textReference) {
            this.textReference = textReference;
            return this;
        }

        public double getNumberReference() {
            return this.numberReference;
        }

        public OtherManagmentDataBuilder withNumberReference(double numberReference) {
            this.numberReference = numberReference;
            return this;
        }

        public Date getDateReference() {
            return this.dateReference;
        }

        public OtherManagmentDataBuilder withDateReference(Date dateReference) {
            this.dateReference = dateReference;
            return this;
        }
        
        public OtherManagmentData build() {
            return new OtherManagmentData(this);
        }
    }

}
