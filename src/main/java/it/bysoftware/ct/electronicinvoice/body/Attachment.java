/**
 *
 */
package it.bysoftware.ct.electronicinvoice.body;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = {"name", "compressionAlgorithm", "format", "description",
    "attachment"})
public class Attachment {

    private String name;

    private String compressionAlgorithm;

    private String format;

    private String description;

    private String attachment;

    private Attachment() {
        super();
    }

    private Attachment(AttachmentBuilder builder) {
        this();
        this.name = builder.name;
        this.compressionAlgorithm = builder.compressionAlgorithm;
        this.format = builder.format;
        this.description = builder.description;
        this.attachment = builder.attachment;
    }

    /**
     * @return the name
     */
    @XmlElement(name = "NomeAttachment")
    public String getName() {
        return this.name;
    }

    /**
     * @return the compression algorithm
     */
    @XmlElement(name = "AlgoritmoCompressione")
    public String getCompressionAlgorithm() {
        return compressionAlgorithm;
    }

    /**
     * @return the attachment format
     */
    @XmlElement(name = "FormatoAttachment")
    public String getFormat() {
        return format;
    }

    /**
     * @return the attachment description
     */
    @XmlElement(name = "DescrizioneAttachment")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @return Base64 encoded attachment content.
     */
    @XmlElement(name = "Attachment")
    public String getAttachment() {
        return attachment;
    }

    public static class AttachmentBuilder {

        private final String name;
        private transient String format;
        private transient String description;
        private final String attachment;
        private transient String compressionAlgorithm;

        public AttachmentBuilder(String name, String content) {
            this.name = name;
            this.attachment = content;
        }

        public AttachmentBuilder withFormat(String format) {
            this.format = format;
            return this;
        }

        public AttachmentBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public AttachmentBuilder withCompressionAlgorithm(
                String compressionAlgorithm) {
            this.compressionAlgorithm = compressionAlgorithm;
            return this;
        }

        public String getName() {
            return name;
        }

        public String getFormat() {
            return format;
        }

        public String getDescription() {
            return description;
        }

        public String getAttachment() {
            return attachment;
        }

        public String getCompressionAlgorithm() {
            return compressionAlgorithm;
        }

        public Attachment build() {
            return new Attachment(this);
        }
    }

}
