/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.body.goodsandservices;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = {"codeType", "codeValue"})
public class ItemCode {
    
    private String codeType;

    private String codeValue;

    private ItemCode() {
        super();
    }

    public ItemCode(String codeType, String codValue) {
        this();
        this.codeType = codeType;
        this.codeValue = codValue;
    }

    /**
     * Get the value of codeValue
     *
     * @return the value of codeValue
     */
    @XmlElement(name = "CodiceValore")
    public String getCodeValue() {
        return this.codeValue;
    }

    /**
     * Set the value of codeValue
     *
     * @param codeValue new value of codeValue
     */
    public void setCodeValue(String codeValue) {
        this.codeValue = codeValue;
    }

    /**
     * Get the value of codeType
     *
     * @return the value of codeType
     */
    @XmlElement(name = "CodiceTipo")
    public String getCodeType() {
        return this.codeType;
    }

    /**
     * Set the value of codeType
     *
     * @param codeType new value of codeType
     */
    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

}
