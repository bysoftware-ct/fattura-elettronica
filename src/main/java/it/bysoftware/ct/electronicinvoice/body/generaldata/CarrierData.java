/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.bysoftware.ct.electronicinvoice.body.generaldata;

import it.bysoftware.ct.electronicinvoice.header.CompanyData;
import it.bysoftware.ct.electronicinvoice.header.FiscalId;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Aliseo-G
 */
@XmlType(propOrder = {"fiscalId", "fiscalCode", "companyData",
    "drivingLicenseNumber"})
public final class CarrierData {

    /**
     *
     */
    private transient FiscalId fiscalId;

    /**
     *
     */
    private transient String fiscalCode;
    
    /**
     *
     */
    private transient CompanyData companyData;
    
    /**
     *
     */
    private transient String drivingLicenseNumber;

    protected CarrierData() {
        super();
    }

    private CarrierData(final CarrierDataBuilder builder) {
        this();
        this.fiscalId = builder.fiscalId;
        this.fiscalCode = builder.fiscalCode;
        this.companyData = builder.companyData;
        this.drivingLicenseNumber = builder.drivingLicenseNumber;
    }

    @XmlElement(name = "NumeroLicenzaGuida")
    public String getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }

    @XmlElement(name = "IdFiscaleIVA")
    public FiscalId getFiscalId() {
        return fiscalId;
    }
    
    @XmlElement(name = "CodiceFiscale")
    public String getFiscalCode() {
        return fiscalCode;
    }

    @XmlElement(name = "Anagrafica")
    public CompanyData getCompanyData() {
        return companyData;
    }

    public static class CarrierDataBuilder {

        private transient String drivingLicenseNumber;
        private final FiscalId fiscalId;
        private transient String fiscalCode;
        private final CompanyData companyData;

        public CarrierDataBuilder(FiscalId fiscalId, CompanyData data) {
            this.fiscalId = fiscalId;
            this.companyData = data;
        }

        public CarrierDataBuilder withFiscalCode(String fiscalCode) {
            this.fiscalCode = fiscalCode;
            return this;
        }
        
        public CarrierDataBuilder withDrivingLicenseNumber(
                String drivingLicenseNumber) {
            this.drivingLicenseNumber = drivingLicenseNumber;
            return this;
        }

        /**
         * Builds the {@link CustomerData}.
         *
         * @return the {@link CustomerData}
         */
        public final CarrierData build() {
            return new CarrierData(this);
        }
    }
}
