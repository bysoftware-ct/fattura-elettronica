/**
 *
 */
package it.bysoftware.ct.electronicinvoice.body.generaldata;

import it.bysoftware.ct.electronicinvoice.header.Address;
import it.bysoftware.ct.electronicinvoice.utils.Constants;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = {"carrier", "transportMeans", "transportCausal",
    "packages", "description", "unitMes", "grossWeight", "netWeight",
    "pickTimestamp", "transportStartDate", "returnType", "returnAddress",
    "deliveryTimestamp"})
public final class CarriageData {

    /**
     * Number formatter.
     */
    private final transient DecimalFormat nFormatter;
    
    /**
     * Date formatter.
     */
    private final transient SimpleDateFormat dFormatter;

    /**
     * Timestamp formatter.
     */
    private final transient SimpleDateFormat tFormatter;

    /**
     *
     */
    private transient CarrierData carrier;

    /**
     *
     */
    private transient String transportMeans;

    /**
     *
     */
    private transient String transportCausal;

    /**
     *
     */
    private transient int packages;

    /**
     *
     */
    private transient String description;

    /**
     *
     */
    private transient String unitMes;

    /**
     *
     */
    private transient double grossWeight;

    /**
     *
     */
    private transient double netWeight;

    /**
     *
     */
    private transient Date pickTimestamp;

    /**
     *
     */
    private transient Date transportStartDate;

    /**
     *
     */
    private transient String returnType;

    /**
     *
     */
    private transient Address returnAddress;

    /**
     *
     */
    private transient Date deliveryTimestamp;

    /**
     * This class cannot be directly instantiated, please refer to
     * {@link CarriageDataBuilder}.
     */
    private CarriageData() {
        super();
        this.nFormatter = new DecimalFormat(Constants.DOUBLE_PATTERN,
                DecimalFormatSymbols.getInstance(Locale.US));
        this.dFormatter = new SimpleDateFormat(Constants.DATE_PATTERN,
                Locale.US);
        this.tFormatter = new SimpleDateFormat(Constants.DATE_PATTERN,
                Locale.US);
    }

    /**
     *
     * @param builder the builder
     */
    private CarriageData(final CarriageDataBuilder builder) {
        this();
        this.carrier = builder.carrier;
        this.transportMeans = builder.transportMeans;
        this.transportCausal = builder.transportCausal;
        this.packages = builder.packages;
        this.description = builder.description;
        this.unitMes = builder.unitMes;
        this.grossWeight = builder.grossWeight;
        this.netWeight = builder.netWeight;
        this.pickTimestamp = builder.pickTimestamp;
        this.transportStartDate = builder.transportStartDate;
        this.returnType = builder.returnType;
        this.returnAddress = builder.returnAddress;
        this.deliveryTimestamp = builder.deliveryTimestamp;
    }

    /**
     * Returns the Carrier business Data.
     *
     * @return Carrier business Data
     */
    @XmlElement(name = "DatiAnagraficiVettore")
    public CarrierData getCarrier() {
        return this.carrier;
    }

    /**
     * Returns the transport means.
     *
     * @return the transport means.
     */
    @XmlElement(name = "MezzoTrasporto")
    public String getTransportMeans() {
        return this.transportMeans;
    }

    /**
     * Returns transport causal.
     *
     * @return the transport causal
     */
    @XmlElement(name = "CausaleTrasporto")
    public String getTransportCausal() {
        return this.transportCausal;
    }

    /**
     * Returns pakacges number.
     *
     * @return the packages number
     */
    @XmlElement(name = "NumeroColli")
    public String getPackages() {
        if (this.packages != 0) {
            return String.valueOf(this.packages);
        } else {
            return null;
        }
    }

    /**
     * Returns description.
     *
     * @return the description
     */
    @XmlElement(name = "Descrizione")
    public String getDescription() {
        return this.description;
    }

    /**
     * Returns the measure unit.
     *
     * @return the measure unit
     */
    @XmlElement(name = "UnitaMisuraPeso")
    public String getUnitMes() {
        return this.unitMes;
    }

    /**
     * Returns the gross weight.
     *
     * @return the gross weight
     */
    @XmlElement(name = "PesoLordo")
    public String getGrossWeight() {
        if (this.grossWeight != 0) {
            return this.nFormatter.format(this.grossWeight);
        } else {
            return null;
        }
    }

    /**
     * Returns the net weight.
     *
     * @return the net weight
     */
    @XmlElement(name = "PesoNetto")
    public String getNetWeight() {
        if (this.netWeight != 0) {
            return this.nFormatter.format(this.netWeight);
        } else {
            return null;
        }
    }

    /**
     * Returns the pick timestamp.
     *
     * @return the pick timestamp
     */
    @XmlElement(name = "DataOraRitiro")
    public String getPickTimestamp() {
        if (this.pickTimestamp != null) {
            return this.tFormatter.format(this.pickTimestamp);
        } else {
            return null;
        }
    }

    /**
     * Returns the start transportation date.
     *
     * @return the strat transportation date
     */
    @XmlElement(name = "DataInizioTrasporto")
    public String getTransportStartDate() {
        if (this.transportStartDate != null) {
            return this.dFormatter.format(this.transportStartDate);
        } else {
            return null;
        }
    }

    /**
     * Returns return type.
     *
     * @return the return type.
     */
    @XmlElement(name = "TipoResa")
    public String getReturnType() {
        return this.returnType;
    }

    /**
     * Returns the return address.
     *
     * @return the return address
     */
    @XmlElement(name = "IndirizzoResa")
    public Address getReturnAddress() {
        return returnAddress;
    }

    /**
     * Returns the delivery timestamp.
     *
     * @return the delivery timestamp
     */
    @XmlElement(name = "IndirizzoResa")
    public String getDeliveryTimestamp() {
        if (this.deliveryTimestamp != null) {
            return this.tFormatter.format(this.deliveryTimestamp);
        } else {
            return null;
        }
    }

    /**
     *
     */
    public static final class CarriageDataBuilder {

        /**
         * The carrier Data.
         */
        private transient CarrierData carrier;
        /**
         * The transport means.
         */
        private transient String transportMeans;
        /**
         * The transport causal.
         */
        private transient String transportCausal;
        /**
         * The pacakges number.
         */
        private transient int packages;
        /**
         * The description.
         */
        private transient String description;
        /**
         * The mesure unit.
         */
        private transient String unitMes;
        /**
         * The gross weight.
         */
        private transient double grossWeight;
        /**
         * The net weight.
         */
        private transient double netWeight;
        /**
         * The pick timestamp.
         */
        private transient Date pickTimestamp;
        /**
         * The return type.
         */
        private transient String returnType;
        /**
         * The net return address.
         */
        private transient Address returnAddress;
        /**
         * the delivery timestamp.
         */
        private transient Date deliveryTimestamp;
        /**
         * The transport start date.
         */
        private transient Date transportStartDate;

        /**
         * Creates a new {@link CarriageDataBuilder}.
         */
        public CarriageDataBuilder() {
        }

        /**
         * Sets the carrieda data.
         *
         * @param data carrier data.
         * @return the builder
         */
        public CarriageDataBuilder withCarrier(final CarrierData data) {
            this.carrier = data;
            return this;
        }

        /**
         * Sets the transport means.
         *
         * @param mean the transort means
         * @return the builder
         */
        public CarriageDataBuilder withTransportMeans(final String mean) {
            this.transportMeans = mean;
            return this;
        }

        /**
         * Sets the transport causal.
         *
         * @param causal transport causal
         * @return the builder
         */
        public CarriageDataBuilder withTransportCausal(final String causal) {
            this.transportCausal = causal;
            return this;
        }

        /**
         * Sets the packages number.
         *
         * @param num packages number
         * @return thebuilder
         */
        public CarriageDataBuilder withPackages(final int num) {
            this.packages = num;
            return this;
        }

        /**
         * Sets the desciption.
         *
         * @param descr the description
         * @return the description
         */
        public CarriageDataBuilder withDescription(final String descr) {
            this.description = descr;
            return this;
        }

        /**
         * Sets the measeure unit.
         *
         * @param unit the measeure unit
         * @return the builder
         */
        public CarriageDataBuilder withUnitMes(final String unit) {
            this.unitMes = unit;
            return this;
        }

        /**
         * Sets the gross weight.
         *
         * @param weight the gross weight
         * @return the builder
         */
        public CarriageDataBuilder withGrossWeight(final double weight) {
            this.grossWeight = weight;
            return this;
        }

        /**
         * Sets the net weight.
         *
         * @param weight net weight
         * @return the builder
         */
        public CarriageDataBuilder withNetWeight(final double weight) {
            this.netWeight = weight;
            return this;
        }

        /**
         * Sets the pick time.
         *
         * @param time the pick time
         * @return the builder
         */
        public CarriageDataBuilder withPickTimestamp(final Date time) {
            this.pickTimestamp = time;
            return this;
        }

        /**
         * Sets the return type.
         *
         * @param type the return type
         * @return the builder
         */
        public CarriageDataBuilder withReturnType(final String type) {
            this.returnType = type;
            return this;
        }

        /**
         * Sets the return address.
         *
         * @param address the return address.
         * @return the builder
         */
        public CarriageDataBuilder withReturnAddress(final Address address) {
            this.returnAddress = address;
            return this;
        }

        /**
         * Sets the delivery time.
         *
         * @param time the delivery time
         * @return the builder
         */
        public CarriageDataBuilder withDeliveryTimestamp(final Date time) {
            this.deliveryTimestamp = time;
            return this;
        }

        /**
         * Sets the trasport start date.
         *
         * @param date trasport start date.
         * @return the builder
         */
        public CarriageDataBuilder withTransportStartDate(final Date date) {
            this.transportStartDate = date;
            return this;
        }

        /**
         * Actually builds the {@link CarriageData}.
         *
         * @return the {@link CarriageData}
         */
        public CarriageData build() {
            return new CarriageData(this);
        }

    }

}
