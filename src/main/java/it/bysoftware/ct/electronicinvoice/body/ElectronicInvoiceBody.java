/**
 * 
 */
package it.bysoftware.ct.electronicinvoice.body;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Aliseo-G
 *
 */
@XmlType(propOrder = { "generalData", "goodsServices", "vehicles", "payments",
        "attachments" })
public final class ElectronicInvoiceBody {

    /**
     *  
     */
    private transient GeneralData generalData;

    /**
     * 
     */
    private transient GoodsServices goodsServices;

    /**
     * 
     */
    private transient Vehicles vehicles;

    /**
     * 
     */
    private transient List<PaymentData> payments;

    /**
     * 
     */
    private transient List<Attachment> attachments;

    /**
     * 
     */
    private ElectronicInvoiceBody() {
        super();
    }

    /**
     * Private constructor, the only way to build an
     * {@link ElectronicInvoiceBody} is using
     * {@link ElectronicInvoiceBodyBuilder}.
     * 
     * @param builder
     *            the builder
     */
    protected ElectronicInvoiceBody(
            final ElectronicInvoiceBodyBuilder builder) {
        this();
        this.generalData = builder.getGeneralData();
        this.goodsServices = builder.getGoodsServices();
        this.attachments = builder.getAttachments();
        this.payments = builder.getPayments();
        this.vehicles = builder.getVehicles();
    }

    /**
     * @return the generalData
     */
    @XmlElement(name = "DatiGenerali")
    public GeneralData getGeneralData() {
        return this.generalData;
    }

    /**
     * @return the goodsServices
     */
    @XmlElement(name = "DatiBeniServizi")
    public GoodsServices getGoodsServices() {
        return this.goodsServices;
    }

    /**
     * @return the vehicles
     */
    @XmlElement(name = "DatiVeicoli")
    public Vehicles getVehicles() {
        return this.vehicles;
    }

    /**
     * @return the payments
     */
    @XmlElement(name = "DatiPagamento")
    public List<PaymentData> getPayments() {
        return this.payments;
    }

    /**
     * @return the attachments
     */
    @XmlElement(name = "Allegati")
    public List<Attachment> getAttachments() {
        return this.attachments;
    }

    /**
     * @author Aliseo-G
     *
     */
    public static final class ElectronicInvoiceBodyBuilder {
        /**
         *  
         */
        private final transient GeneralData generalData;

        /**
         * 
         */
        private final transient GoodsServices goodsServices;

        /**
         * 
         */
        private transient Vehicles vehicles;

        /**
         * 
         */
        private transient List<PaymentData> payments;

        /**
         * 
         */
        private transient List<Attachment> attachments;

        /**
         * @return the generalData
         */
        private GeneralData getGeneralData() {
            return this.generalData;
        }

        /**
         * @return the goodsServices
         */
        private GoodsServices getGoodsServices() {
            return this.goodsServices;
        }

        /**
         * @return the vehicles
         */
        private Vehicles getVehicles() {
            return this.vehicles;
        }

        /**
         * @return the payments
         */
        private List<PaymentData> getPayments() {
            return this.payments;
        }

        /**
         * @return the attachments
         */
        private List<Attachment> getAttachments() {
            return this.attachments;
        }

        /**
         * Creates a new {@link ElectronicInvoiceBodyBuilder}.
         * 
         * @param data
         *            the {@link GeneralData}
         * @param goods
         *            the {@link GoodsServices}
         */
        public ElectronicInvoiceBodyBuilder(final GeneralData data,
                final GoodsServices goods) {
            super();
            this.generalData = data;
            this.goodsServices = goods;
        }

        /**
         * Sets the specified vehicles to the
         * {@link ElectronicInvoiceBodyBuilder}.
         * 
         * @param vehicle
         *            the vehicles to set
         * @return an {@link ElectronicInvoiceBodyBuilder} with the vehicle set
         */
        public ElectronicInvoiceBodyBuilder withVehicles(
                final Vehicles vehicle) {
            this.vehicles = vehicle;
            return this;
        }

        /**
         * Sets the specified payments to the
         * {@link ElectronicInvoiceBodyBuilder}.
         * 
         * @param payment
         *            the payments to set
         * @return an {@link ElectronicInvoiceBodyBuilder} with the payments set
         */
        public ElectronicInvoiceBodyBuilder withPayment(final PaymentData payment) {
            final List<PaymentData> list = new ArrayList<PaymentData>();
            list.add(payment);
            this.payments = list;
            return this;
        }

        /**
         * Sets the specified payments to the
         * {@link ElectronicInvoiceBodyBuilder}.
         * 
         * @param list
         *            the payments to set
         * @return an {@link ElectronicInvoiceBodyBuilder} with the payments set
         */
        public ElectronicInvoiceBodyBuilder withPayments(
                final List<PaymentData> list) {
            this.payments = list;
            return this;
        }

        /**
         * Sets the specified attachment to the
         * {@link ElectronicInvoiceBodyBuilder}.
         * 
         * @param attachment
         *            the attachment to set
         * @return an {@link ElectronicInvoiceBodyBuilder} with the attachments
         *         set
         */
        public ElectronicInvoiceBodyBuilder withAttachment(
                final Attachment attachment) {
            final List<Attachment> list = new ArrayList<Attachment>();
            list.add(attachment);
            this.attachments = list;
            return this;
        }

        /**
         * Sets the specified attachments to the
         * {@link ElectronicInvoiceBodyBuilder}.
         * 
         * @param list
         *            the attachments to set
         * @return an {@link ElectronicInvoiceBodyBuilder} with the attachments
         *         set
         */
        public ElectronicInvoiceBodyBuilder withAttachments(
                final List<Attachment> list) {
            this.attachments = list;
            return this;
        }

        /**
         * Builds the actual {@link ElectronicInvoiceBody}.
         * 
         * @return {@link ElectronicInvoiceBody} instance
         */
        public ElectronicInvoiceBody build() {
            return new ElectronicInvoiceBody(this);
        }
    }

}
